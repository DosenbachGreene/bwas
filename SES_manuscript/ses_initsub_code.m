%% Menhattan plot colored
figure; hold on;
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable.Effect_sizes,'vector','descend');
for s = 1:length(varnames)
    k = scatter(ones(size(rsfcdisc,2),1)+s-1,allbrbxphewas(:,sortidx(s)),'jitter','on','jitteramount',0.3);
    k.Marker = '.'; k.SizeData = 30;
    if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
        k.MarkerFaceColor = [116 196 118]./255;
        k.MarkerEdgeColor = [116 196 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
        k.MarkerFaceColor = [166 206 227]./255;
        k.MarkerEdgeColor = [166 206 227]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
        k.MarkerFaceColor = [10 107 10]./255;
        k.MarkerEdgeColor = [10 107 10]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
        k.MarkerFaceColor = [158 154 200]./255;
        k.MarkerEdgeColor = [158 154 200]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
        k.MarkerFaceColor = [227 26 28]./255;
        k.MarkerEdgeColor = [227 26 28]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
        k.MarkerFaceColor = [0 0 118]./255;
        k.MarkerEdgeColor = [0 0 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
        k.MarkerFaceColor = [255 255 127]./255;
        k.MarkerEdgeColor = [255 255 127]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
        k.MarkerFaceColor = [251 154 153]./255;
        k.MarkerEdgeColor = [251 154 153]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
        k.MarkerFaceColor = [106 61 154]./255;
        k.MarkerEdgeColor = [106 61 154]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
        k.MarkerFaceColor = [253 191 111]./255;
        k.MarkerEdgeColor = [253 191 111]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k.MarkerFaceColor = [31 120 180]./255;
        k.MarkerEdgeColor = [31 120 180]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
        k.MarkerFaceColor = [0 128 128]./255;
        k.MarkerEdgeColor = [0 128 128]./255;
   end
   j = scatter(s,top1all(sortidx(s)),'filled');
   j.Marker = 'd'; j.MarkerEdgeColor = [0 0 0]; j.MarkerFaceColor = [0 0 0];
end
set(gca,'FontSize',8,'LineWidth',2,'FontName','Arial')
ylim([-.01 .26]); xticks([1:length(varnames)]); xticklabels(varnames(sortidx)); xtickangle(90);

%%
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Effect_Size','descend');
idxes = strcmpi('Socioeconomic',rsfcbx_allefffectstable.Category);
topord = top1all(rsfcdiscphewasoutput.sortidx);
% rsfcdisc_brainmapcorrs_abs_sortedbycat = rsfcdisc_brainmapcorrs_abs_sorted(rsfcbx_allefffectstable.vector,rsfcbx_allefffectstable.vector);
%topord = topord(rsfcbx_allefffectstable.vector);
figure;hold on; 
for s = 1:length(idxes(idxes==0))
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(topord(s),rsfcdisc_brainmapcorrs_abs_sorted(s,1));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end

% fit dbl exp
f = fit(topord',rsfcdisc_brainmapcorrs_abs_sorted(:,1),'exp2');
t=plot(f); t.Color = [0 0 0]; t.LineWidth = 2.5;
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
xlim([.0525 .1225])

%% one way anova of category by brain 


%% brain with cog
% LME Multiple regression - RSFC
behthreefive = innerjoin(behthree,behfive,'Keys','Subject');
behthreefive = innerjoin(behthreefive,behdisc,'Keys','Subject');

varstoinc = [
    find(strcmpi('TotalCogAbility',behthreefive.Properties.VariableNames) == 1) , ...
    find(strcmpi('FamilyIncome',behthreefive.Properties.VariableNames) == 1) , ... 
    find(strcmpi('Age',behthreefive.Properties.VariableNames) == 1) , ... 
    find(strcmpi('Sex',behthreefive.Properties.VariableNames) == 1) , ... 
    find(strcmpi('FD',behthreefive.Properties.VariableNames) == 1) ];
behthreefive.reshist_addr1_coi_z_se_nat = cellfun( @str2double, behthreefive.reshist_addr1_coi_z_se_nat);
func = @zscore;
test = varfun(func,behthreefive,'InputVariables',varstoinc);
behthreefive = [behthreefive test];

nanidxadi = find(isnan(behthreefive.reshist_addr1_coi_z_se_nat) == 1);

behthreefive(nanidxadi,:) = [];
rsfcdiscsescog = rsfcdiscthree;
rsfcdiscsescog(nanidxadi,:) = [];

rsfcsescoeffs_adj = zeros(size(rsfcdiscsescog,2),1); 
rsfccogcoeffs_adj = zeros(size(rsfcdiscsescog,2),1);
rsfccogcoeffs_unadj = zeros(size(rsfcdiscsescog,2),1);
rsfcsescoeffs_unadj = zeros(size(rsfcdiscsescog,2),1);
rsfcseststat_adj = zeros(size(rsfcdiscsescog,2),1); 
rsfccogtstat_adj = zeros(size(rsfcdiscsescog,2),1);
rsfccogtstat_unadj = zeros(size(rsfcdiscsescog,2),1);
rsfcseststat_unadj = zeros(size(rsfcdiscsescog,2),1);
for i = 39286:size(rsfcdiscsescog,2)
    this = zscore(rsfcdiscsescog(:,i));
    subsdisc = addvars(behthreefive,this);
    mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_TotalCogAbility + reshist_addr1_coi_z_se_nat + zscore_FamilyIncome + zscore_FD + (1|abcd_lt01_site_id_l)');
    rsfcsescoeffs_adj(i,1) = table2array(mdl.Coefficients(2,2));
    rsfccogcoeffs_adj(i,1) = table2array(mdl.Coefficients(3,2));
    rsfcseststat_adj(i,1) = table2array(mdl.Coefficients(2,4));
    rsfccogtstat_adj(i,1) = table2array(mdl.Coefficients(3,4));
    mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_TotalCogAbility + zscore_FD + (1|abcd_lt01_site_id_l)');
    rsfccogcoeffs_unadj(i,1) = table2array(mdl.Coefficients(2,2));
    rsfccogtstat_unadj(i,1) = table2array(mdl.Coefficients(2,4));
%     mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_sespc1 + zscore_FD + (1|abcd_lt01_site_id_l)');
%     rsfcsescoeffs_unadj(i,1) = table2array(mdl.Coefficients(5,2));
%     rsfcseststat_unadj(i,1) = table2array(mdl.Coefficients(5,4));
end
corr(rsfcseststat_unadj,rsfccogtstat_unadj)


% Make scatter plot of the association between t-statistics
figure; hold on;
h = histogram(abs([rsfccogtstat_unadj]),edges);
h.FaceColor = [150 150 150]./255;
h.EdgeColor = [150 150 150]./255;
z = histogram(abs([rsfccogtstat_adj]),edges);
z.FaceColor = [65 171 93]./255;
z.EdgeColor = [65 171 93]./255;
z.FaceAlpha = .4; z.EdgeAlpha = .4;
xlabel('|t-statistic|')
ylabel('Frequency')
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/rsfc_cog_sesadj_v_unadj','png')

all_unadj = [ctcogtstat_unadj ; rsfccogtstat_unadj];
all_adj = [ctcogtstat_adj ; rsfccogtstat_adj];

% Percent of associations that decrease effect size 
unadj = abs(all_unadj(abs(all_unadj)>3));
adj = abs(all_adj(abs(all_unadj)>3));

difference = adj - unadj; 100.*(length(find(difference<0)) / length(difference))

% Percent of adj associations that are sig unadj & >= unadj value
sig_noinflation = 100*(length(find(difference>=0)) / length(difference));

% Percent of adj associations that are no longer sig 
nosig = 100*(length(find(adj<3)) / length(adj));

% Percent of adj associations that are sig unadj & < unadj value (the
% remainder)
sig_wasinflated = 100-sig_noinflation-nosig;

disp(['Percent significant and not inflated = ' num2str(sig_noinflation)])
disp(['Percent significant but were inflated = ' num2str(sig_wasinflated)])
disp(['Percent no longer significant = ' num2str(nosig)])


%% Primary individual differences 
pmtable = innerjoin(behone,behtwo,'Keys','Subject');
pmtable = innerjoin(pmtable,behthree,'Keys','Subject');
pmtable = innerjoin(pmtable,behfour,'Keys','Subject');
pmtable = innerjoin(pmtable,behfive,'Keys','Subject');
pmtable = innerjoin(pmtable,behdisc,'Keys','Subject');


nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
variables = [pmtable.Age pmtable.Sex pmtable.FD cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend), pmtable.FamilyIncome pmtable.sleepdisturb1_p_behdisc cellfun(@str2double,pmtable.demo_prnt_marital_v2b)];
variables(nanidx,:) = [];
% rsfcpmt = rsfcdiscthree;
%rsfcpmt(nanidx,:) = [];
pmtable(nanidx,:) = [];
variables(:,7) = variables(:,7) + variables(:,8); variables(:,8) = [];
variables = zscore(variables);
variables = array2table(variables);
variables.Properties.VariableNames{1} = 'Age';
variables.Properties.VariableNames{2} = 'Sex';
variables.Properties.VariableNames{3} = 'FD';
variables.Properties.VariableNames{4} = 'ses';
variables.Properties.VariableNames{5} = 'iq';
variables.Properties.VariableNames{6} = 'p';
variables.Properties.VariableNames{7} = 'screentime';
variables.Properties.VariableNames{8} = 'fi';
variables.Properties.VariableNames{9} = 'sd';
variables.Properties.VariableNames{10} = 'marital';
variables = addvars(variables,pmtable.abcd_lt01_site_id_l);
variables.Properties.VariableNames{11} = 'site';

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
variables = addvars(variables,brainscores(:,1));
variables.Properties.VariableNames{12} = 'pc1';

% replication set 
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);
variablesrep = [pmtablerep.Age pmtablerep.Sex pmtablerep.FD cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend), pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)];
variablesrep(nanidxrep,:) = [];
% rsfcpmt = rsfcdiscthree;
%pmtablerep(nanidxrep,:) = [];
variablesrep(:,7) = variablesrep(:,7) + variablesrep(:,8); variablesrep(:,8) = [];
variablesrep = zscore(variablesrep);
variablesrep = array2table(variablesrep);
variablesrep.Properties.VariableNames{1} = 'Age';
variablesrep.Properties.VariableNames{2} = 'Sex';
variablesrep.Properties.VariableNames{3} = 'FD';
variablesrep.Properties.VariableNames{4} = 'ses';
variablesrep.Properties.VariableNames{5} = 'iq';
variablesrep.Properties.VariableNames{6} = 'p';
variablesrep.Properties.VariableNames{7} = 'screentime';
variablesrep.Properties.VariableNames{8} = 'fi';
variablesrep.Properties.VariableNames{9} = 'sd';
variablesrep.Properties.VariableNames{10} = 'marital';
variablesrep = addvars(variablesrep,pmtablerep.abcd_lt01_site_id_l(~nanidxrep));
variablesrep.Properties.VariableNames{11} = 'site';



% All variables included
mdl = fitlme(variables, ' pc1 ~ Age + Sex + FD + ses + iq + screentime + sd  + (1|site)')

% IQ
mdliq = fitlme(variables, ' pc1 ~ Age + Sex + FD + iq + (1|site)')

% all edges
rsfcseststat_adj = zeros(size(rsfcpmt,2),1); 
rsfciqtstat_adj = zeros(size(rsfcpmt,2),1);
rsfcsttstat_adj = zeros(size(rsfcpmt,2),1);
rsfcsdtstat_adj = zeros(size(rsfcpmt,2),1);

rsfcseststat_unadj = zeros(size(rsfcpmt,2),1); 
rsfciqtstat_unadj = zeros(size(rsfcpmt,2),1); 
rsfcsttstat_unadj = zeros(size(rsfcpmt,2),1); 
rsfcsdtstat_unadj = zeros(size(rsfcpmt,2),1); 

% z=parpool(5);
for i = 1:size(rsfcpmt,2)
    disp([' On edge ' num2str(i)])
    this = zscore(rsfcpmt(:,i));
    subsdisc = addvars(variables,this);
    mdl = fitlme(subsdisc,'this ~ Age + Sex + iq + ses + FD + screentime + sd + (1|site)');
    rsfcseststat_adj(i,1) = table2array(mdl.Coefficients(5,2));
    rsfciqtstat_adj(i,1) = table2array(mdl.Coefficients(6,2));
    rsfcsttstat_adj(i,1) = table2array(mdl.Coefficients(7,2));
    rsfcsdtstat_adj(i,1) = table2array(mdl.Coefficients(8,2));
    
    mdl = fitlme(subsdisc,'this ~ Age + Sex + FD + ses + (1|site)');
    rsfcseststat_unadj(i,1) = table2array(mdl.Coefficients(5,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + FD + iq + (1|site)');
    rsfciqtstat_unadj(i,1) = table2array(mdl.Coefficients(5,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + FD + screentime + (1|site)');
    rsfcsttstat_unadj(i,1) = table2array(mdl.Coefficients(5,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + FD + sd +(1|site)');
    rsfcsdtstat_unadj(i,1) = table2array(mdl.Coefficients(5,2));
end
% delete(z)

% Compare tstats (99th %ile) of adjusted and unadjusted
sesadj = prctile(abs(rsfcseststat_adj),99)
sesunadj = prctile(abs(rsfcseststat_unadj),99)
iqadj = prctile(abs(rsfciqtstat_adj),99)
iqunadj = prctile(abs(rsfciqtstat_unadj),99)
stadj = prctile(abs(rsfcsttstat_adj),99)
stunadj = prctile(abs(rsfcsttstat_unadj),99)
sdadj = prctile(abs(rsfcsdtstat_adj),99)
sdunadj = prctile(abs(rsfcsdtstat_unadj),99)

% straight up r 
[~,~,riq] = regress(variables.iq,[ones(size(variables,1),1) variables.ses variables.sd variables.screentime]);
for i = 1:size(rsfcpmt,2)
   iqunc(i,1) = corr(rsfcpmt(:,i),variables.iq);
   iqcor(i,1) = corr(rsfcpmt(:,i),riq);
end
prctile(abs(iqunc),99)
prctile(abs(iqcor),99)

% straight up r 
[~,~,rst] = regress(variables.p,[ones(size(variables,1),1) variables.ses variables.sd variables.screentime]);
for i = 1:size(rsfcpmt,2)
   punc(i,1) = corr(rsfcpmt(:,i),variables.p);
   pcor(i,1) = corr(rsfcpmt(:,i),rst);
end
prctile(abs(punc),99)
prctile(abs(pcor),99)



% bar charts of unadj vs. adj values
figure; hold on;

    h = bar(1,.13);
    h.FaceColor = [31 120 180]./255;
    h = bar(2,.12);
    h.FaceColor = [10 107 10]./255;
    h = bar(3,.10);
    h.FaceColor = [0 0 118]./255;
    h = bar(4,.09);
    h.FaceColor = [116 196 118]./255;
    
    h = bar(7,.10);
    h.FaceColor = [31 120 180]./255;
    h = bar(8,.09);
    h.FaceColor = [10 107 10]./255;
    h = bar(9,.08);
    h.FaceColor = [0 0 118]./255;
    h = bar(10,.07);
    h.FaceColor = [116 196 118]./255;

xlim([0 11])
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

% Correlation matrix
% stem plot
cmap = inferno(100); %cmap = flipud(cmap);
variablesarray = table2array(variables(:,[4 9 7 5 6]));
varcorr = abs(corrcoef(variablesarray));
uidx = find(triu(varcorr));
varcorr(uidx) = 0;
cmap(1,:) = [1 1 1];
figure; imagesc(varcorr);colormap(cmap);caxis([0.01 .35]);colorbar
box('off')
xticklabels([' ']);
yticklabels([' ']);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

%% CCA all vars 
load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')

%[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);


pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected ];
[A B r U V] = canoncorr(x,y); r

% replication
% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
    pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
    pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
    pmtablerep.nihtbx_reading_uncorrected ];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% regress normed adi and family income out of cog phenotypes
% fit discovery set model (in sample)
% now also regress ses from cognition and refit discovery model 
[~,~,cogr1] = regress(pmtable.nihtbx_cardsort_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr2] = regress(pmtable.nihtbx_flanker_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr3] = regress(pmtable.nihtbx_list_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr4] = regress(pmtable.nihtbx_pattern_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr5] = regress(pmtable.nihtbx_picture_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr6] = regress(pmtable.nihtbx_picvocab_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);
[~,~,cogr7] = regress(pmtable.nihtbx_reading_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b)]);

% [~,~,cogr1] = regress(pmtable.nihtbx_cardsort_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr2] = regress(pmtable.nihtbx_flanker_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr3] = regress(pmtable.nihtbx_list_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr4] = regress(pmtable.nihtbx_pattern_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr5] = regress(pmtable.nihtbx_picture_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr6] = regress(pmtable.nihtbx_picvocab_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);
% [~,~,cogr7] = regress(pmtable.nihtbx_reading_uncorrected,[ones(size(pmtable,1),1) cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtable.demo_comb_income_v2b) , cellfun(@str2double,pmtable.stq_y_ss_weekday) , pmtable.sleepdisturb1_p_behdisc]);


x = brainscores(:,1:fn);
y = [cogr1 , cogr2 , cogr3 ,cogr4 , cogr5 , cogr6 , cogr7];
[A,B,r] = canoncorr(x,y); r

% apply to new out of sample, (also not including ses)
% regress from 
% now also regress ses from cognition and refit discovery model 
% nanidxr = sum(isnan([cellfun(@str2double,pmtabler.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtabler.demo_comb_income_v2b)]),2);
% pmtabler(nanidxr==1,:) = [];
[~,~,cogr1] = regress(pmtablerep.nihtbx_cardsort_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr2] = regress(pmtablerep.nihtbx_flanker_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr3] = regress(pmtablerep.nihtbx_list_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr4] = regress(pmtablerep.nihtbx_pattern_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr5] = regress(pmtablerep.nihtbx_picture_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr6] = regress(pmtablerep.nihtbx_picvocab_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr7] = regress(pmtablerep.nihtbx_reading_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);

xadj = xun;
yadj = [cogr1 , cogr2 , cogr3 ,cogr4, cogr5 , cogr6 , cogr7];
oosr = corr(xadj*A(:,1),yadj*B(:,1));
oosr_adj = diag(oosr);
rsfc_cca_cog_sesadj = oosr_adj(1);

figure;
hold on;
h = scatter(zscore(xun*A(:,1)) , zscore(yun*B(:,1))); 
h.Marker = '.'; h.SizeData = 150; 
h.MarkerFaceColor = [100 100 100]./255;
h.MarkerEdgeColor = [100 100 100]./255;
j = lsline; j(1).Color = 'k'; j(1).LineWidth = 2.5;

h = scatter(zscore(xadj*A(:,1)) , zscore(yadj*B(:,1))); 
h.Marker = '.'; h.SizeData = 150; 
h.MarkerFaceColor = [116 196 118]./255;
h.MarkerEdgeColor = [116 196 118]./255;
h.MarkerEdgeAlpha = .3; h.MarkerFaceAlpha = .3;
j = lsline; j(1).Color = [0 141 0]./255; j(1).LineWidth = 2.5; 
j(2).Color = 'k'; j(2).LineWidth = 2.5;
ylim([-6 7])
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

% regress ses from both brain and cognition in test set
bsr = [];
for i = 1:fn
   [~,~,bsr(:,i)] = regress(brainscores_rep(:,i) , [ones(size(brainscores_rep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); 
end
[~,~,cogr1] = regress(pmtablerep.nihtbx_cardsort_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr2] = regress(pmtablerep.nihtbx_flanker_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr3] = regress(pmtablerep.nihtbx_list_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr4] = regress(pmtablerep.nihtbx_pattern_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr5] = regress(pmtablerep.nihtbx_picture_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr6] = regress(pmtablerep.nihtbx_picvocab_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);
[~,~,cogr7] = regress(pmtablerep.nihtbx_reading_uncorrected,[ones(size(pmtablerep,1),1) cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]); % , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , pmtablerep.sleepdisturb1_p_behdisc]);

xadj = bsr;
yadj = [cogr1 , cogr2 , cogr3 ,cogr4, cogr5 , cogr6 , cogr7];
oosr = corr(xadj*A(:,1),yadj*B(:,1));
oosr_adj = diag(oosr)



%% high vs low ses cca 
% low ses predicting high ses rsfc with iq 
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmth = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.25,:);
YH = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r;

% Apply out of sample 
rsfcrep = rsfcpmth;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YH*B(:,1));
oosr = diag(oosr);

% begin plotting
figure;subplot(2,3,3)
h=scatter(zscore(xun*A(:,1)),zscore(YH*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% high ses predicting high ses 
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YH = Y(variables.ses>.75,:);
YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); 

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr);

%plot
subplot(2,3,6)
h=scatter(zscore(xun*A(:,1)),zscore(YHrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);


% high  predicting low 
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YH = Y(variables.ses>.75,:);
YLrep = Yrep(variablesrep.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH);

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr);

%plot
subplot(2,3,5)
h=scatter(zscore(xun*A(:,1)),zscore(YLrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% low predicting low
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.25,:);
YLrep = Yrep(variablesrep.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); 

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr);

%plot
subplot(2,3,2)
h=scatter(zscore(xun*A(:,1)),zscore(YLrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Train on high ses discovery, test on full replication
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); 

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr);

subplot(2,3,4)
h=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Train on low ses discovery, test on full replication
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YLdisc = Y(variables.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YLdisc); 

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr);

subplot(2,3,1)
h=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

for i = 1:6
    subplot(2,3,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
end

%% cortical thickness 2x3

load('pmtablectdisc.mat')
rsfcpmt=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_disc.txt');
load('pmtablectrep.mat')
rsfcpmtrep=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_rep.txt');

pmtablerep = pmtablectrep;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.FamilyIncome ]),2);
nanidx = logical(nanidx);
variables = [pmtable.Age pmtable.Sex cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.FamilyIncome];
variables(nanidx,:) = [];

rsfcpmt(nanidx,:) = [];
pmtable(nanidx,:) = [];
variables = zscore(variables);
variables = array2table(variables);
variables.Properties.VariableNames{1} = 'Age';
variables.Properties.VariableNames{2} = 'Sex';
variables.Properties.VariableNames{3} = 'ses';
variables.Properties.VariableNames{4} = 'iq';
variables.Properties.VariableNames{5} = 'fi';

variables = addvars(variables,pmtable.abcd_lt01_site_id_l);
variables.Properties.VariableNames{6} = 'site';


% replication set 
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.FamilyIncome ]),2);
nanidxrep = logical(nanidxrep);
variablesrep = [pmtablerep.Age pmtablerep.Sex cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.FamilyIncome];
variablesrep(nanidxrep,:) = [];

rsfcpmtrep(nanidxrep,:) = [];
pmtablerep(nanidxrep,:) = [];
variablesrep = zscore(variablesrep);
variablesrep = array2table(variablesrep);
variablesrep.Properties.VariableNames{1} = 'Age';
variablesrep.Properties.VariableNames{2} = 'Sex';
variablesrep.Properties.VariableNames{3} = 'ses';
variablesrep.Properties.VariableNames{4} = 'iq';
variablesrep.Properties.VariableNames{5} = 'fi';

variablesrep = addvars(variablesrep,pmtablerep.abcd_lt01_site_id_l);
variablesrep.Properties.VariableNames{6} = 'site';

rsfcpmt = table2array(rsfcpmt);
rsfcpmtrep = table2array(rsfcpmtrep);


% low ses predicting high ses rsfc with iq 
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmth = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.25,:);
YH = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r;

% Apply out of sample 
rsfcrep = rsfcpmth;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YH*B(:,1));
oosr = diag(oosr)

% begin plotting
figure;subplot(2,3,3)
h=scatter(zscore(xun*A(:,1)),zscore(YH*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% high ses predicting high ses 
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YH = Y(variables.ses>.75,:);
YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); 

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)

%plot
subplot(2,3,6)
h=scatter(zscore(xun*A(:,1)),zscore(YHrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);


% high  predicting low 
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YH = Y(variables.ses>.75,:);
YLrep = Yrep(variablesrep.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

%plot
subplot(2,3,5)
h=scatter(zscore(xun*A(:,1)),zscore(YLrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% low predicting low
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.25,:);
YLrep = Yrep(variablesrep.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

%plot
subplot(2,3,2)
h=scatter(zscore(xun*A(:,1)),zscore(YLrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Train on high ses discovery, test on full replication
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); rinsamp

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

subplot(2,3,4)
h=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Train on low ses discovery, test on full replication
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YLdisc = Y(variables.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YLdisc); 

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

subplot(2,3,1)
h=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255; 
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

for i = 1:6
    subplot(2,3,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
end




%%
% low predicting low .. controlling for ses 
rsfcpmtl = rsfcpmt(variables.ses<-.25,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.25,:);
YLrep = Yrep(variablesrep.ses<-.25,:);

sesn = variables.ses(variables.ses<-.25);
sesfi = variables.fi(variables.ses<-.25);
sesnrep = variablesrep.ses(variablesrep.ses<-.25);
sesfirep = variablesrep.fi(variablesrep.ses<-.25);

for i = 1:size(YL,2)
    [~ , ~ , YL(:,i)] = regress(YL(:,i) , [ones(size(YL,1),1) sesn sesfi]);
    [~ , ~ , YLrep(:,i)] = regress(YLrep(:,i) , [ones(size(YLrep,1),1) sesnrep sesfirep]);
end


[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)


% high predicting high 
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YH = Y(variables.ses>.75,:);
YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)

% low predicting high .. controlling for ses 
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YL = Y(variables.ses<-.5,:);
YHrep = Yrep(variablesrep.ses>.75,:);

sesn = variables.ses(variables.ses<-.5);
sesfi = variables.fi(variables.ses<-.5);
sesnrep = variablesrep.ses(variablesrep.ses>.75);
sesfirep = variablesrep.fi(variablesrep.ses>.75);

for i = 1:size(YL,2)
    [~ , ~ , YL(:,i)] = regress(YL(:,i) , [ones(size(YL,1),1) sesn sesfi]);
    [~ , ~ , YHrep(:,i)] = regress(YHrep(:,i) , [ones(size(YHrep,1),1) sesnrep sesfirep]);
end

YL = zscore(YL);
YHrep = zscore(YHrep);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)



%% Sex foil 

% First is female predicting male (< 0 = male , > 0 = female)
rsfcpmtf = rsfcpmt(variables.Sex>0,:);
rsfcpmtm = rsfcpmtrep(variablesrep.Sex<0,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YF = Y(variables.Sex>0,:);
YM = Yrep(variablesrep.Sex<0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtf);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YF); 

% Apply out of sample 
rsfcrep = rsfcpmtm;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YM*B(:,1));
oosr = diag(oosr);

% begin plotting
figure; hold on;
subplot(2,2,2)
h=scatter(zscore(xun*A(:,1)),zscore(YM*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);


% Sex foil - Female discovery predicting female replicaiton
rsfcpmtf = rsfcpmt(variables.Sex>0,:);
rsfcpmtrepf = rsfcpmtrep(variablesrep.Sex>0,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

Yf = Y(variables.Sex>0,:);
Yfrep = Yrep(variablesrep.Sex>0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtf);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Yf); 

% Apply out of sample 
rsfcrep = rsfcpmtrepf;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Yfrep*B(:,1));
oosr = diag(oosr);

subplot(2,2,1)
h=scatter(zscore(xun*A(:,1)),zscore(Yfrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Male predicting female
rsfcpmtm = rsfcpmt(variables.Sex<0,:);
rsfcpmtf = rsfcpmtrep(variablesrep.Sex>0,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YM = Y(variables.Sex<0,:);
YF = Yrep(variablesrep.Sex>0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtm);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YM); 

% Apply out of sample 
rsfcrep = rsfcpmtrepf;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YF*B(:,1));
oosr = diag(oosr);

subplot(2,2,3)
h=scatter(zscore(xun*A(:,1)),zscore(YF*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

% Sex foil - Male discovery predicting male replicaiton
rsfcpmtm = rsfcpmt(variables.Sex<0,:);
rsfcpmtrepm = rsfcpmtrep(variablesrep.Sex<0,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

Ym = Y(variables.Sex<0,:);
Ymrep = Yrep(variablesrep.Sex<0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtm);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Ym); 

% Apply out of sample 
rsfcrep = rsfcpmtrepm;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Ymrep*B(:,1));
oosr = diag(oosr);

subplot(2,2,4)
h=scatter(zscore(xun*A(:,1)),zscore(Ymrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(oosr));
xlim([-6 6]); ylim([-6 6]);

for i = 1:4
    subplot(2,2,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
end

%% Sex foil for predicting ses 
% First is female predicting male (< 0 = male , > 0 = female)
rsfcpmtf = rsfcpmt(variables.Sex>0,:);
rsfcpmtm = rsfcpmtrep(variablesrep.Sex<0,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi ];

YF = Y(variables.Sex>0,:);
YM = Yrep(variablesrep.Sex<0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtf);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YF); 

% Apply out of sample 
rsfcrep = rsfcpmtm;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YM*B(:,1));
oosr = diag(oosr);

% begin plotting
figure; hold on;
subplot(2,2,2)
h=scatter(zscore(xun*A(:,1)),zscore(YM*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [107 174 214]./255;
h.MarkerEdgeColor = [8 48 107]./255;
title(num2str(oosr));
xlim([-5 5]); ylim([-5 5]);


% Sex foil - Female discovery predicting female replicaiton
rsfcpmtf = rsfcpmt(variables.Sex>0,:);
rsfcpmtrepf = rsfcpmtrep(variablesrep.Sex>0,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi ];

Yf = Y(variables.Sex>0,:);
Yfrep = Yrep(variablesrep.Sex>0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtf);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Yf); 

% Apply out of sample 
rsfcrep = rsfcpmtrepf;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Yfrep*B(:,1));
oosr = diag(oosr);

subplot(2,2,1)
h=scatter(zscore(xun*A(:,1)),zscore(Yfrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [107 174 214]./255;
h.MarkerEdgeColor = [8 48 107]./255;
title(num2str(oosr));
xlim([-5 5]); ylim([-5 5]);

% Male predicting female
rsfcpmtm = rsfcpmt(variables.Sex<0,:);
rsfcpmtf = rsfcpmtrep(variablesrep.Sex>0,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi ];

YM = Y(variables.Sex<0,:);
YF = Yrep(variablesrep.Sex>0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtm);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YM); 

% Apply out of sample 
rsfcrep = rsfcpmtrepf;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YF*B(:,1));
oosr = diag(oosr);

subplot(2,2,3)
h=scatter(zscore(xun*A(:,1)),zscore(YF*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [107 174 214]./255;
h.MarkerEdgeColor = [8 48 107]./255;
title(num2str(oosr));
xlim([-5 5]); ylim([-5 5]);

% Sex foil - Male discovery predicting male replicaiton
rsfcpmtm = rsfcpmt(variables.Sex<0,:);
rsfcpmtrepm = rsfcpmtrep(variablesrep.Sex<0,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi ];

Ym = Y(variables.Sex<0,:);
Ymrep = Yrep(variablesrep.Sex<0,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtm);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Ym); 

% Apply out of sample 
rsfcrep = rsfcpmtrepm;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Ymrep*B(:,1));
oosr = diag(oosr);

subplot(2,2,4)
h=scatter(zscore(xun*A(:,1)),zscore(Ymrep*B(:,1))); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [107 174 214]./255;
h.MarkerEdgeColor = [8 48 107]./255;
title(num2str(oosr));
xlim([-5 5]); ylim([-5 5]);

for i = 1:4
    subplot(2,2,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
end



%%

% Train on full discovery, test on low ses 
rsfcpmt = rsfcpmt;
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YLrep = Yrep(variablesrep.ses<-.25,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

% Train on full discovery, test on low ses .. correct for ses 
rsfcpmt = rsfcpmt;
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.25,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YLrep = Yrep(variablesrep.ses<-.25,:);

sesn = variables.ses;
sesfi = variables.fi;
sesnrep = variablesrep.ses(variablesrep.ses<-.25);
sesfirep = variablesrep.fi(variablesrep.ses<-.25);

for i = 1:size(YL,2)
    [~ , ~ , Y(:,i)] = regress(Y(:,i) , [ones(size(Y,1),1) sesn sesfi]);
    [~ , ~ , YLrep(:,i)] = regress(YLrep(:,i) , [ones(size(YLrep,1),1) sesnrep sesfirep]);
end

YL = zscore(YL);
YHrep = zscore(YHrep);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)



% Train on full discovery, test on high ses 
rsfcpmt = rsfcpmt;
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)

% Train on high ses discovery, test on full replication
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); rinsamp

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% plot - if you remove outlier corr is still 0.3971
figure;ins=scatter(zscore(xinsamp(:,1)),zscore(yinsamp(:,1))); ij = lsline;
ins.Marker = '.'; ins.SizeData = 200; 
ins.MarkerFaceColor = [116 198 118]./255;
ins.MarkerEdgeColor = [116 198 118]./255;
ins.MarkerFaceAlpha = .4;
ij.LineWidth = 2.5; ij.Color = 'k';
ylim([-5 3]);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
figure;
oos=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); oj = lsline;
oos.Marker = '.'; oos.SizeData = 200; 
oos.MarkerFaceColor = [0 109 44]./255;
oos.MarkerEdgeColor = [0 109 44]./255;
oos.MarkerFaceAlpha = .4;
oj.LineWidth = 2.5; oj.Color = 'k';
ylim([-5 3]);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
figure;ins=scatter(zscore(xinsamp(xinsamp(:,1)>-5,1)),zscore(yinsamp(xinsamp(:,1)>-5,1))); ij = lsline;
ins.Marker = '.'; ins.SizeData = 200; 
ins.MarkerFaceColor = [116 198 118]./255;
ins.MarkerEdgeColor = [116 198 118]./255;
ins.MarkerFaceAlpha = .4;
ij.LineWidth = 2.5; ij.Color = 'k';
xlim([-4 4]); ylim([-5 3]);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

% subsaample replication set w matched sample size to further probe
% generalizability (n=579 in each) 
Y = YHdisc;
Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];
for i = 1:100
    idx = randperm(size(rsfcpmtrep,1));
    rsfcsubrep = rsfcpmtrep(idx(1:size(rsfcpmth,1)),:);
    thisYrep = Yrep(idx(1:size(rsfcpmth,1)),:);
    % Apply out of sample 
    rsfcrep = rsfcsubrep;
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    oosr = corr(xun*A(:,1),thisYrep*B(:,1));
    oosr = diag(oosr);
    rperm(i,1) = oosr;
end
mean(rperm)

% Null model for high ses discovery sample
% Train on high ses discovery, test on full replication
rsfcpmth = rsfcpmt(variables.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
% randomly permute the behavioral data 1,000x to build null
for i = 1:1000
    idx = datasample(1:size(YHdisc,1),size(YHdisc,1))';
    YHdiscperm = YHdisc(idx);
    [~,~,rhsesperm(i,1)] = canoncorr(X,YHdiscperm);
end
figure;h=histogram(rhsesperm);
h.FaceColor = [.3 .3 .3];
hold on;
t=xline(rinsamp(1)); t.LineWidth = 2; t.Color = [0 .7 0];
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
box('off')

% Train on low ses discovery, test on full replication
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YLdisc = Y(variables.ses<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YLdisc); r

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% Train on full discovery, test on full replication (100x)
Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

X = [rsfcpmt ; rsfcpmtrep];
Y = [Y ; Yrep];
sescov = [variables.ses variables.fi ; variablesrep.ses variablesrep.fi];

for i = 1:100
    idx = randperm(size(Y,1))';
    Xins = X(idx(1:2289),:);
    Yins = Y(idx(1:2289),:);
    Xoos = X(idx(2290:end),:);
    Yoos = Y(idx(2290:end),:);
    [coeffs,brainscores,~,~,explained,mu] = pca(Xins);
    rsfc_varexplained = cumsum(explained);
    % Using components accounting for 20% variance exaplained for prediction
    fn = find(rsfc_varexplained>20,1);
    thisx = brainscores(:,1:fn);
    [A, B, r] = canoncorr(thisx,Yins); 

    % Apply out of sample 
    brainscores_rep = (Xoos - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);

    oosr = corr(xun*A(:,1),Yoos*B(:,1));
    oosrboot(i,1) = diag(oosr);
    
    % regress ses and repeat
    sesins = sescov(idx(1:2289),:);
    sesoos = sescov(idx(2290:end),:);
    
    [~,~,residins(:,1)] = regress(Yins(:,1) , [ones(size(Yins,1),1) , sesins]);
    [~,~,residins(:,2)] = regress(Yins(:,2) , [ones(size(Yins,1),1) , sesins]);
    [~,~,residins(:,3)] = regress(Yins(:,3) , [ones(size(Yins,1),1) , sesins]);
    [~,~,residins(:,4)] = regress(Yins(:,4) , [ones(size(Yins,1),1) , sesins]);
    [~,~,residins(:,5)] = regress(Yins(:,5) , [ones(size(Yins,1),1) , sesins]);
    [~,~,residins(:,6)] = regress(Yins(:,6) , [ones(size(Yins,1),1) , sesins]);
    
    [~,~,residoos(:,1)] = regress(Yoos(:,1) , [ones(size(Yoos,1),1) , sesoos]);
    [~,~,residoos(:,2)] = regress(Yoos(:,2) , [ones(size(Yoos,1),1) , sesoos]);
    [~,~,residoos(:,3)] = regress(Yoos(:,3) , [ones(size(Yoos,1),1) , sesoos]);
    [~,~,residoos(:,4)] = regress(Yoos(:,4) , [ones(size(Yoos,1),1) , sesoos]);
    [~,~,residoos(:,5)] = regress(Yoos(:,5) , [ones(size(Yoos,1),1) , sesoos]);
    [~,~,residoos(:,6)] = regress(Yoos(:,6) , [ones(size(Yoos,1),1) , sesoos]);
    
    % train
    [A, B, r] = canoncorr(thisx,residins); 
    % test
    oosr = corr(xun*A(:,1),residoos*B(:,1));
    oosrboot(i,2) = diag(oosr);
    
end

figure; hold on
for k = 1 : size(oosrboot,1)
    h=plot([1 2], [oosrboot(k,1), oosrboot(k,2)]);
    h.Color = [.8 .8 .8];
    h.LineWidth = 1.5;
    i = scatter(1,oosrboot(k,1));
    i.SizeData = 100;
    i.MarkerEdgeColor = [.5 .5 .5];
    j = scatter(2,oosrboot(k,2));
    j.SizeData = 100;
    j.MarkerEdgeColor = [116 196 118]./255;
end
xlim([.9 2.1])
h=plot([1 2], [.35, .21]);
h.Color = [.2 .2 .2];
h.LineWidth = 1.5;
i = scatter(1,.35);
i.SizeData = 200;
i.MarkerEdgeColor = [0 0 0];
i.Marker = 'd';
i.MarkerFaceColor = [0 0 0];
j = scatter(2,.21);
j.SizeData = 200;
j.MarkerEdgeColor = [0 104 0]./255;
j.Marker = 'd';
j.MarkerFaceColor = [0 104 0]./255;
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial','xtick',[])


%% Train/test brain with sleep 

% train low, test high
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmth = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ variables.sd ];

Yrep = [ variablesrep.sd ];

YL = Y(variables.ses<-.5,:);
YH = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmth;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YH*B(:,1));
oosr = diag(oosr)

% train low, test high
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.sd ];

Yrep = [ variablesrep.sd ];

YL = Y(variables.ses<-.5,:);
YLrep = Yrep(variablesrep.ses<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

% train high, test low
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.sd ];

Yrep = [ variablesrep.sd ];

YH = Y(variables.ses>.75,:);
YL = Yrep(variablesrep.ses<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YL*B(:,1));
oosr = diag(oosr)

% train high, test high
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ variables.sd ];

Yrep = [ variablesrep.sd ];

YH = Y(variables.ses>.75,:);
YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)

% train low, test low, controlling for ses (adi + fi)
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.sd ];

Yrep = [ variablesrep.sd ];
YL = Y(variables.ses<-.5,:);
YLrep = Yrep(variablesrep.ses<-.5,:);

sesn = variables.ses(variables.ses<-.5);
sesfi = variables.fi(variables.ses<-.5);
sesnrep = variablesrep.ses(variablesrep.ses<-.5);
sesfirep = variablesrep.fi(variablesrep.ses<-.5);


[~ , ~ , YL] = regress(YL , [ones(size(YL,1),1) sesn sesfi]);
[~ , ~ , YLrep(:,i)] = regress(YLrep(:,i) , [ones(size(YLrep,1),1) sesnrep sesfirep]);



[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

%% Train/test brain with sleep 

% train low, test high
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmth = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ variables.screentime ];

Yrep = [ variablesrep.screentime ];

YL = Y(variables.ses<-.5,:);
YH = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmth;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YH*B(:,1));
oosr = diag(oosr)

% train low, test low
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.screentime ];

Yrep = [ variablesrep.screentime ];

YL = Y(variables.ses<-.5,:);
YLrep = Yrep(variablesrep.ses<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

% train high, test low
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.screentime ];

Yrep = [ variablesrep.screentime ];

YH = Y(variables.ses>.75,:);
YL = Yrep(variablesrep.ses<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YL*B(:,1));
oosr = diag(oosr)

% train high, test high
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.ses>.75,:);

Y = [ variables.screentime ];

Yrep = [ variablesrep.screentime ];

YH = Y(variables.ses>.75,:);
YHrep = Yrep(variablesrep.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)

% train low, test low, controlling for ses (adi + fi)
rsfcpmtl = rsfcpmt(variables.ses<-.5,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.ses<-.5,:);

Y = [ variables.screentime ];

Yrep = [ variablesrep.screentime ];
YL = Y(variables.ses<-.5,:);
YLrep = Yrep(variablesrep.ses<-.5,:);

sesn = variables.ses(variables.ses<-.5);
sesfi = variables.fi(variables.ses<-.5);
sesnrep = variablesrep.ses(variablesrep.ses<-.5);
sesfirep = variablesrep.fi(variablesrep.ses<-.5);


[~ , ~ , YL] = regress(YL , [ones(size(YL,1),1) sesn sesfi]);
[~ , ~ , YLrep(:,i)] = regress(YLrep(:,i) , [ones(size(YLrep,1),1) sesnrep sesfirep]);



[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)


% Full sample
Y = [ variables.screentime ];
Yrep = [ variablesrep.screentime ];
[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% Full sample grab w/ 600 n in each 
for i = 1:100
    idx = randperm(size(variables,1));
    idxrep = randperm(size(variablesrep,1));
    Y = [ variables.screentime ];
    Yrep = [ variablesrep.screentime ];
    rsfcsub = rsfcpmt(idx(1:600),:);
    rsfcsubrep = rsfcpmtrep(idxrep(1:600),:);
    Y = Y(idx(1:600),:);
    Yrep = Yrep(idxrep(1:600),:);
    [coeffs,brainscores,~,~,explained,mu] = pca(rsfcsub);
    rsfc_varexplained = cumsum(explained);
    % Using components accounting for 20% variance exaplained for prediction
    fn = find(rsfc_varexplained>20,1);
    X = brainscores(:,1:fn);
    [A, B, r] = canoncorr(X,Y); 

    % Apply out of sample 
    rsfcrep = rsfcsubrep;
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    oosr = corr(xun*A(:,1),Yrep*B(:,1));
    oosr = diag(oosr);
    rperm(i,1) = oosr;
end
mean(rperm)



%% Train/test brain with ses, using h v l intelligence 

% train low, test high
rsfcpmtl = rsfcpmt(variables.iq < -.5,:);
rsfcpmth = rsfcpmtrep(variablesrep.iq>.5,:);

Y = [ variables.ses variables.fi];

Yrep = [ variablesrep.ses variablesrep.fi];

YL = Y(variables.iq<-.5,:);
YH = Yrep(variablesrep.iq>.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmth;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YH*B(:,1));
oosr = diag(oosr)

% train low, test low
rsfcpmtl = rsfcpmt(variables.iq<-.5,:);
rsfcpmtrepl = rsfcpmtrep(variablesrep.iq<-.5,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi];

YL = Y(variables.iq<-.5,:);
YLrep = Yrep(variablesrep.iq<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r

% Apply out of sample 
rsfcrep = rsfcpmtrepl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YLrep*B(:,1));
oosr = diag(oosr)

% train high, test low
rsfcpmth = rsfcpmt(variables.iq>.5,:);
rsfcpmtl = rsfcpmtrep(variablesrep.iq<-.5,:);

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi];

YH = Y(variables.iq>.5,:);
YL = Yrep(variablesrep.iq<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtl;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YL*B(:,1));
oosr = diag(oosr)

% train high, test high
rsfcpmth = rsfcpmt(variables.iq>.5,:);
rsfcpmtreph = rsfcpmtrep(variablesrep.iq>.5,:);

Y = [ variables.ses variables.fi];

Yrep = [ variablesrep.ses variablesrep.fi ];

YH = Y(variables.iq>.5,:);
YHrep = Yrep(variablesrep.iq>.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r

% Apply out of sample 
rsfcrep = rsfcpmtreph;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),YHrep*B(:,1));
oosr = diag(oosr)


% Full sample
Y = [ variables.ses variables.fi ];
Yrep = [ variablesrep.ses variablesrep.fi ];
[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% train high iq test full 
rsfcpmth = rsfcpmt(variables.iq>.5,:);

Y = [ variables.ses variables.fi];

Yrep = [ variablesrep.ses variablesrep.fi ];

YH = Y(variables.iq>.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YH); r
% apply out of sample
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% train low iq test full 
rsfcpmtl = rsfcpmt(variables.iq<-.5,:);

Y = [ variables.ses variables.fi];

Yrep = [ variablesrep.ses variablesrep.fi ];

YL = Y(variables.iq<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YL); r
% apply out of sample
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% Full sample grab w/ 600 n in each 
for i = 1:100
    idx = randperm(size(variables,1));
    %idxrep = randperm(size(variablesrep,1));
    Y = [ variables.ses variables.fi ];
    Yrep = [ variablesrep.ses variablesrep.fi ];
    rsfcsub = rsfcpmt(idx(1:700),:);
    %rsfcsubrep = rsfcpmtrep(idxrep(1:700),:);
    Y = Y(idx(1:700),:);
    %Yrep = Yrep(idxrep(1:700),:);
    [coeffs,brainscores,~,~,explained,mu] = pca(rsfcsub);
    rsfc_varexplained = cumsum(explained);
    % Using components accounting for 20% variance exaplained for prediction
    fn = find(rsfc_varexplained>20,1);
    X = brainscores(:,1:fn);
    [A, B, r] = canoncorr(X,Y); 

    % Apply out of sample 
    rsfcrep = rsfcpmtrep;
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    oosr = corr(xun*A(:,1),Yrep*B(:,1));
    oosr = diag(oosr);
    rperm(i,1) = oosr;
end
mean(rperm)

% full sample ses controlling for iq 
Y = [ variables.ses variables.fi ];
Yrep = [ variablesrep.ses variablesrep.fi ];
for i = 1:2
    [~,~,Y(:,i)] = regress(Y(:,i) , [ones(size(Y,1),1) variables.iq]);
    [~,~,Yrep(:,i)] = regress(Yrep(:,i) , [ones(size(Yrep,1),1) variablesrep.iq]);    
end
[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,Y); r

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

%%
% Train brain with ses on high ses discovery, test on full replication
rsfcpmth = rsfcpmt(variables.ses>.75,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ variables.ses variables.fi ];

Yrep = [ variablesrep.ses variablesrep.fi ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, r] = canoncorr(X,YHdisc); r

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

oosr = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

%% regress out sleep and scren time + ses 

[~,~,cogr1] = regress(variables.iq,[ones(size(variables,1),1) variables.ses , variables.fi , variables.sd , variables.screentime]);
[A,B,r] = canoncorr(X,cogr1); r



%% Connectome based prediction model - RSFC
addpath(genpath('/data/nil-bluearc/GMT/Scott/CPM-master/'))
% load data
load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')
nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
pmtable(nanidx,:) = [];

% cognition - unadjusted
allmats_cpm = [rsfcpmt ; rsfcpmtrep]';
cogpc1_cpm = [pmtable.nihtbx_totalcomp_uncorrected ; pmtablerep.nihtbx_totalcomp_uncorrected]';
adinormed = [cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]';
faminc = [cellfun(@str2double , pmtable.demo_comb_income_v2b) ; cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]';

% Split halves 
[ypred,performacecog_rsfc] = cpm_main(allmats_cpm,cogpc1_cpm);
% cognition - adjusted
[~,~,r] = regress(cogpc1_cpm',[ones(size(cogpc1_cpm,2),1) adinormed' faminc']);
[adjperf,padj] = corr(ypred,r);

% Permute 1000x
performacecog_rsfc_perm = []; performacecog_rsfc_wses_perm =[];
for p = 1:100
    disp(['On interation ' num2str(p)])
    %thisidx = datasample(1:size(allmats_cpm,2),size(allmats_cpm,2));
    thisidx = randperm(size(allmats_cpm,2));
    mats = allmats_cpm(:,thisidx);
    cogcpm = cogpc1_cpm(thisidx);
    adinormedcpm = adinormed(thisidx);
    faminccpm = faminc(thisidx);
    %unadj
    [thisypred,thisp] = cpm_main(mats,cogcpm);
    performacecog_rsfc_perm(p,1) = thisp(1);
    %adj
    [~,~,rperm] = regress(cogcpm',[ones(size(cogcpm,2),1) adinormedcpm' faminccpm']);
    performacecog_rsfc_wses_perm(p,1) = corr(thisypred,rperm);
end
set1 = performacecog_rsfc_perm;
set2 = performacecog_rsfc_wses_perm;
figure; hold on 
for k = 1 : length(set1)
    h=plot([1 2], [set1(k), set2(k)]);
    h.Color = [.8 .8 .8];
    h.LineWidth = 1.5;
    i = scatter(1,set1(k));
    i.SizeData = 100;
    i.MarkerEdgeColor = [.5 .5 .5];
    j = scatter(2,set2(k));
    j.SizeData = 100;
    j.MarkerEdgeColor = [116 196 118]./255;
end
xlim([.9 2.1])
h=plot([1 2], [performacecog_rsfc(1), adjperf]);
h.Color = [.2 .2 .2];
h.LineWidth = 1.5;
i = scatter(1,performacecog_rsfc(1));
i.SizeData = 200;
i.MarkerEdgeColor = [0 0 0];
i.Marker = 'd';
i.MarkerFaceColor = [0 0 0];
j = scatter(2,adjperf);
j.SizeData = 200;
j.MarkerEdgeColor = [0 104 0]./255;
j.Marker = 'd';
j.MarkerFaceColor = [0 104 0]./255;
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
ylim([.10 .29])


%%
load('pmtablectdisc.mat');
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmt_ctdisc.mat');
rsfcpmt = rsfcdisc; clear rsfcdisc
pmtablerep = load('pmtablectrep.mat');
rsfcpmtrep=load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmt_ctrep.mat');
pmtablerep = pmtablerep.pmtable;
rsfcpmtrep = rsfcpmtrep.rsfcdisc;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility ]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility]),2);
nanidxrep = logical(nanidxrep);

rsfcpmt(nanidx == 1,:) = [];
rsfcpmtrep(nanidxrep == 1,:) = [];
pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

% cognition - unadjusted
allmats_cpm = [rsfcpmt ; rsfcpmtrep]';
cogpc1_cpm = [pmtable.nihtbx_totalcomp_uncorrected ; pmtablerep.nihtbx_totalcomp_uncorrected]';
adinormed = [cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]';
faminc = [cellfun(@str2double , pmtable.demo_comb_income_v2b) ; cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]';

% Split halves 
[ypred,performancecog] = cpm_main(allmats_cpm,cogpc1_cpm);
% cognition - adjusted
[~,~,r] = regress(cogpc1_cpm',[ones(size(cogpc1_cpm,2),1) adinormed' faminc']);
[adjperf,padj] = corr(ypred,r);

% Permute 1000x
performacecog_perm = []; performacecog_wses_perm =[];
for p = 1:100
    disp(['On interation ' num2str(p)])
    %thisidx = datasample(1:size(allmats_cpm,2),size(allmats_cpm,2));
    thisidx = randperm(size(allmats_cpm,2));
    mats = allmats_cpm(:,thisidx);
    cogcpm = cogpc1_cpm(thisidx);
    adinormedcpm = adinormed(thisidx);
    faminccpm = faminc(thisidx);
    %unadj
    [thisypred,thisp] = cpm_main(mats,cogcpm);
    performancecog_perm(p,1) = thisp(1);
    %adj
    [~,~,rperm] = regress(cogcpm',[ones(size(cogcpm,2),1) adinormedcpm' faminccpm']);
    performancecog_wses_perm(p,1) = corr(thisypred,rperm);
end
set1 = performancecog_perm;
set2 = performancecog_wses_perm;
figure; hold on 
for k = 1 : length(set1)
    h=plot([1 2], [set1(k), set2(k)]);
    h.Color = [.8 .8 .8];
    h.LineWidth = 1.5;
    i = scatter(1,set1(k));
    i.SizeData = 100;
    i.MarkerEdgeColor = [.5 .5 .5];
    j = scatter(2,set2(k));
    j.SizeData = 100;
    j.MarkerEdgeColor = [116 196 118]./255;
end
xlim([.9 2.1])
h=plot([1 2], [performancecog(1), adjperf]);
h.Color = [.2 .2 .2];
h.LineWidth = 1.5;
i = scatter(1,performancecog(1));
i.SizeData = 200;
i.MarkerEdgeColor = [0 0 0];
i.Marker = 'd';
i.MarkerFaceColor = [0 0 0];
j = scatter(2,adjperf);
j.SizeData = 200;
j.MarkerEdgeColor = [0 104 0]./255;
j.Marker = 'd';
j.MarkerFaceColor = [0 104 0]./255;
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
ylim([.08 .26])


%% Brain maps
addpath('/data/nil-bluearc/GMT/Scott/ABCD/ManhattanProject/ABCD_pitt_washu/Scripts/')
Read_Gordon_Parcel_IDs
% Cog
iq = allbrbxphewas_noabs(:,642);
mat = ones(333);
uidx = find(triu(mat,1));
mat = zeros(333);
mat(uidx) = iq;
iqmat = mat + mat';
%iqmat = zscore(mat);

% SES
% Cog
sesnat = allbrbxphewas_noabs(:,589);
mat = ones(333);
uidx = find(triu(mat,1));
mat = zeros(333);
mat(uidx) = sesnat;
sesmat = mat + mat';
%sesmat = zscore(mat);


sesabs = sum(abs(sesmat))';
iqabs = sum(abs(iqmat))';
corr(sesabs,iqabs)

sesabs = (sesabs - min(sesabs)) ./ (max(sesabs) - min(sesabs));
iqabs = (iqabs - min(iqabs)) ./ (max(iqabs) - min(iqabs));

tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
out = zeros(size(tf.data,1),2);
for r = 1:333
    idx=length(find(tf.data == r));
    % ses
    this = [];
    this = sesabs(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,1) = this;
    % cog 
    this = [];
    this = iqabs(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,2) = this;
end
tf.data(1:59412,1:2) = out;
%tf.data(:,1) = tf.data(:,1) - .01; tf.data(:,1)
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/rsfc_ses_rsfc_cog_parcels_raw_disc_meanzscore',tf)

%mh foil 
for i = 1:size(rsfcdisc,2)
mh(i,1) = corr(rsfcdisc(:,i),behrep.TotalProblems);
end
mat = ones(333);
uidx = find(triu(mat,1));
mat = zeros(333);
mat(uidx) = mh;
mhmat = mat + mat';


mhabs = sum(abs(mhmat))';

mhabs = (mhabs - min(mhabs)) ./ (max(mhabs) - min(mhabs));

tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
out = zeros(size(tf.data,1),1);
for r = 1:333
    idx=length(find(tf.data == r));
    % ses
    this = [];
    this = mhabs(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,1) = this;
end
tf.data(1:59412,1) = out;
%tf.data(:,1) = tf.data(:,1) - .01; tf.data(:,1)
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/rsfc_mh_parcels_raw_rep_meanzscore',tf)

%% category comparison (brain maps)
% Organize by category .. categories organized by top 1% effect size
% average 
% Ranking = socioeconomics, screen time, cognition, demographics,
% culture/environment, physical health, mental health, social adjustment, 
% substance use, parenting, personality, medical history
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Effect_Size','descend');
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Category','ascend');
rsfcdisc_brainmapcorrs_abs_sorted_bycategory = rsfcdisc_brainmapcorrs_abs_sorted(rsfcbx_allefffectstable.vector,rsfcbx_allefffectstable.vector);
% resort by indices
idx1 = find(strcmpi('Socioeconomic' , rsfcbx_allefffectstable.Category)==1);
idx2 = find(strcmpi('Screen Time' , rsfcbx_allefffectstable.Category)==1);
idx3 = find(strcmpi('Cognition' , rsfcbx_allefffectstable.Category)==1);

idx4 = find(strcmpi('Demographics' , rsfcbx_allefffectstable.Category)==1);
idx5 = find(strcmpi('Culture/Environment' , rsfcbx_allefffectstable.Category)==1);
idx6 = find(strcmpi('Physical Health' , rsfcbx_allefffectstable.Category)==1);

idx7 = find(strcmpi('Mental Health' , rsfcbx_allefffectstable.Category)==1);
idx8 = find(strcmpi('Social Adjustment' , rsfcbx_allefffectstable.Category)==1);
idx9 = find(strcmpi('Substance Use' , rsfcbx_allefffectstable.Category)==1);

idx10 = find(strcmpi('Parenting' , rsfcbx_allefffectstable.Category)==1);
idx11 = find(strcmpi('Personality' , rsfcbx_allefffectstable.Category)==1);
idx12 = find(strcmpi('Medical History' , rsfcbx_allefffectstable.Category)==1);

cateffsort = [idx1 ; idx2 ; idx3 ; idx4 ; idx5 ; idx6 ; idx7 ; idx8 ; idx9 ; idx10; idx11 ; idx12];

figure;imagesc(rsfcdisc_brainmapcorrs_abs_sorted_bycategory(cateffsort,cateffsort));colormap(cmap);


sessim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx1,cateffsort(setdiff(1:end,idx1))) ;
stsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx2,cateffsort(setdiff(1:end,idx2)))  ;
cogsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx3,cateffsort(setdiff(1:end,idx3)))  ;

demsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx4,cateffsort(setdiff(1:end,idx4)))  ;
cesim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx5,cateffsort(setdiff(1:end,idx5)))  ;
phsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx6,cateffsort(setdiff(1:end,idx6)))  ;

mhsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx7,cateffsort(setdiff(1:end,idx7)))  ;
sasim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx8,cateffsort(setdiff(1:end,idx8)))  ;
susim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx9,cateffsort(setdiff(1:end,idx9)))  ;

parentsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx10,cateffsort(setdiff(1:end,idx10)))  ;
personsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx11,cateffsort(setdiff(1:end,idx11))) ;
medhistsim = rsfcdisc_brainmapcorrs_abs_sorted_bycategory(idx12,cateffsort(setdiff(1:end,idx12)))  ;

allsim = [sessim(:); stsim(:); cogsim(:); demsim(:); cesim(:); phsim(:); mhsim(:); sasim(:); susim(:); parentsim(:); personsim(:); medhistsim(:)];
allsim(:,2) = zeros(size(allsim,1),1);
allsim(1:length(sessim(:)),2) = 1;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(stsim(:)) ,2 ) = 2;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(cogsim(:)) ,2 ) = 3;

idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(demsim(:)) ,2 ) = 4;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(cesim(:)) ,2 ) = 5;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(phsim(:)) ,2 ) = 6;

idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(mhsim(:)) ,2 ) = 7;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(sasim(:)) ,2 ) = 8;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(susim(:)) ,2 ) = 9;

idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(parentsim(:)) ,2 ) = 10;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(personsim(:)) ,2 ) = 11;
idx = find(allsim(:,2)==0,1);
allsim(idx : idx + length(medhistsim(:)) ,2 ) = 12;

[p tbl, stats] = anova1(allsim(:,1),allsim(:,2))
c = multcompare(stats)

%% 
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/behvars_disc.mat')
lifefactors = [];
for b = 1:size(behvars,2)
    if isa(table2array(behvars(:,b)),'cell')
        thisbeh = cellfun(@str2double, table2array(behvars(:,b)));
    else
        thisbeh = table2array(behvars(:,b));
    end
    lifefactors(:,b) = thisbeh;
end
lifefactorcorrs = zeros(size(behvars,2));
for i = 1:size(behvars,2)
    for j = i+1:size(behvars,2)
        lifefactorcorrs(i,j) = corr(lifefactors(:,i),lifefactors(:,j),'rows','complete');
    end
end
lifefactorcorrs = lifefactorcorrs + lifefactorcorrs';
lifefactorcorrs_sorted = lifefactorcorrs(rsfcdiscphewasoutput.sortidx,rsfcdiscphewasoutput.sortidx);
lifefactorcorrs_abs_sorted = abs(lifefactorcorrs_sorted);
[rsfcbx_allefffectstable sidx] = sortrows(rsfcbx_allefffectstable,'Category','ascend');

lifefactorcorrs_abs_sorted_bycat = lifefactorcorrs_abs_sorted(sidx,sidx);

% resort by indices
idx1 = find(strcmpi('Socioeconomic' , rsfcbx_allefffectstable.Category)==1);
idx2 = find(strcmpi('Screen Time' , rsfcbx_allefffectstable.Category)==1);
idx3 = find(strcmpi('Cognition' , rsfcbx_allefffectstable.Category)==1);

idx4 = find(strcmpi('Demographics' , rsfcbx_allefffectstable.Category)==1);
idx5 = find(strcmpi('Culture/Environment' , rsfcbx_allefffectstable.Category)==1);
idx6 = find(strcmpi('Physical Health' , rsfcbx_allefffectstable.Category)==1);

idx7 = find(strcmpi('Mental Health' , rsfcbx_allefffectstable.Category)==1);
idx8 = find(strcmpi('Social Adjustment' , rsfcbx_allefffectstable.Category)==1);
idx9 = find(strcmpi('Substance Use' , rsfcbx_allefffectstable.Category)==1);

idx10 = find(strcmpi('Parenting' , rsfcbx_allefffectstable.Category)==1);
idx11 = find(strcmpi('Personality' , rsfcbx_allefffectstable.Category)==1);
idx12 = find(strcmpi('Medical History' , rsfcbx_allefffectstable.Category)==1);

cateffsort = [idx1 ; idx2 ; idx3 ; idx4 ; idx5 ; idx6 ; idx7 ; idx8 ; idx9 ; idx10; idx11 ; idx12];

cmap = inferno(256);

figure;imagesc(lifefactorcorrs_abs_sorted_bycat(cateffsort,cateffsort));colormap(cmap);caxis([0 .4]);

%% mimic fig 2c
%topord = top1all(rsfcdiscphewasoutput.sortidx);
% rsfcdisc_brainmapcorrs_abs_sortedbycat = rsfcdisc_brainmapcorrs_abs_sorted(rsfcbx_allefffectstable.vector,rsfcbx_allefffectstable.vector);
%topord = topord(rsfcbx_allefffectstable.vector);
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'vector','ascend');
figure;hold on; 
for s = 1:size(rsfcbx_allefffectstable,1)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(topord(s),rsfcdisc_brainmapcorrs_abs_sorted(s,1));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

figure;hold on; 
for s = 1:size(lifefactorcorrs,2)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(rsfcdisc_brainmapcorrs_abs_sorted(s,1),lifefactorcorrs_abs_sorted(s,1));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

% life factor correlation with ses - effect size in brain
figure;hold on; 
for s = 1:size(rsfcbx_allefffectstable,1)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(rsfcbx_allefffectstable.lf_w_ses(s,1),rsfcbx_allefffectstable.effectsize_ct(s));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')


%% Fig 2a  make brain maps 
for i = 1:size(rsfcpmt,2)
   ses(i,1) = corr(rsfcpmt(:,i),variables.ses); 
   sleep(i,1) = corr(rsfcpmt(:,i),variables.sd); 
   screens(i,1) = corr(rsfcpmt(:,i),variables.screentime); 
   iq(i,1) = corr(rsfcpmt(:,i),variables.iq); 
   p(i,1) = corr(rsfcpmt(:,i),variables.p); 
end

sesmat = zeros(333);
sleepmat = zeros(333);
screenmat = zeros(333);
iqmat = zeros(333);
pmat = zeros(333);
mats = squeeze(ones(333));
uidx = find(triu(mats,1));

% Into matrix form
sesmat(uidx) = ses; 
sleepmat(uidx) = sleep; 
screenmat(uidx) = screens; 
iqmat(uidx) = iq; 
pmat(uidx) = p; 


sesmat = sesmat + sesmat';
sleepmat = sleepmat + sleepmat';
screenmat = screenmat + screenmat';
iqmat = iqmat + iqmat';
pmat = pmat + pmat';

%Into parcel form (rms - mean of the squared values)
for i = 1:333
    if i == 1
        sesrms(i,1) = sum(abs(sesmat(2:end,i))); % exclude 0 diagonal
        sleeprms(i,1) = sum(abs(sleepmat(2:end,i)));
        screenrms(i,1) = sum(abs(screenmat(2:end,i))); 
        iqrms(i,1) = sum(abs(iqmat(2:end,i)));
        prms(i,1) = sum(abs(pmat(2:end,i))); 
    else
        sesrms(i,1) = sum(abs(sesmat([1:i-1 i+1:end],i)));
        sleeprms(i,1) = sum(abs(sleepmat([1:i-1 i+1:end],i)));
        screenrms(i,1) = sum(abs(screenmat([1:i-1 i+1:end],i)));
        iqrms(i,1) = sum(abs(iqmat([1:i-1 i+1:end],i)));
        prms(i,1) = sum(abs(pmat([1:i-1 i+1:end],i)));
    end
end
% norm each to their own max
sesrms = (sesrms - min(sesrms)) ./ range(sesrms);
sleeprms = (sleeprms - min(sleeprms)) ./ range(sleeprms);
screenrms = (screenrms - min(screenrms)) ./ range(screenrms);
iqrms = (iqrms - min(iqrms)) ./ range(iqrms);
prms = (prms - min(prms)) ./ range(prms);


% Figures - parcels
addpath('/data/cn/data1/scripts/CIFTI_RELATED/Resources/read_write_cifti/fileio/')
rmpath('/data/cn/data1/scripts/CIFTI_RELATED/Resources/DEPRECATED/cifti-matlab-master/')

tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
out = zeros(size(tf.data,1),2);
for r = 1:333
    idx=length(find(tf.data == r));
    % ses
    this = [];
    this = sesrms(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,1) = this;
    % sleep 
    this = [];
    this = sleeprms(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,2) = this;
    % screens
    this = [];
    this = screenrms(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,3) = this;
    % iq 
    this = [];
    this = iqrms(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,4) = this;
    % p 
    this = [];
    this = prms(r,1);
    this = repmat(this,idx,1);
    out(tf.data==r,5) = this;
end
tf.data(1:59412,1:5) = out;
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/ses_allvariable_parcels_raw_disc_sum',tf)


%% cortical thickess
load('ctdiscphewasoutput.mat');
load('rsfcdiscphewasoutput.mat');

allcorrsct = [ctdiscphewas.allcorrs(:,rsfcdiscphewasoutput.idx) ctdiscphewas.allcorrs2(:,rsfcdiscphewasoutput.idx2) ...
    ctdiscphewas.allcorrs3(:,rsfcdiscphewasoutput.idx3) , ctdiscphewas.allcorrs4(:,rsfcdiscphewasoutput.idx4) ...
    ctdiscphewas.allcorrs5(:,rsfcdiscphewasoutput.idx5)];

load('rsfcbx_table_discpluscombatplusrep.mat');

allbrbxphewas = abs(allcorrs);

top1allct = prctile(allbrbxphewas,99);
[~,sortidxct] = sort(top1allct,'descend');

top1allct = top1allct(rsfcdiscphewasoutput.sortidx);
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,top1allct'); 
    
% Combine rsfc and ct 
allcorrsfc = abs([rsfcdiscphewasoutput.allcorrs(:,rsfcdiscphewasoutput.idx) rsfcdiscphewasoutput.allcorrs2(:,rsfcdiscphewasoutput.idx2) ...
    rsfcdiscphewasoutput.allcorrs3(:,rsfcdiscphewasoutput.idx3) , rsfcdiscphewasoutput.allcorrs4(:,rsfcdiscphewasoutput.idx4) ...
    rsfcdiscphewasoutput.allcorrs5(:,rsfcdiscphewasoutput.idx5)]);

combined = [allcorrs ; allcorrsfc];
top1allcomb = prctile(combined,99);
[~,sortidxcomb] = sort(top1allcomb,'descend');

top1allcomb = top1allcomb(rsfcdiscphewasoutput.sortidx);
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,top1allcomb'); 
vectorcomb = [1:1:649]';
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,vectorcomb); 

%% cortical thickness replication
%% cortical thickess
load('ctrepphewasoutput.mat');
load('rsfcdiscphewasoutput.mat');

allcorrs = [ctrepphewasoutput.allcorrs(:,rsfcdiscphewasoutput.idx) ctrepphewasoutput.allcorrs2(:,rsfcdiscphewasoutput.idx2) ...
    ctrepphewasoutput.allcorrs3(:,rsfcdiscphewasoutput.idx3) , ctrepphewasoutput.allcorrs4(:,rsfcdiscphewasoutput.idx4) ...
    ctrepphewasoutput.allcorrs5(:,rsfcdiscphewasoutput.idx5)];

load('rsfcbx_table_discpluscombatplusrep.mat');

allbrbxphewas = abs(allcorrs);

top1allct = prctile(allbrbxphewas,99);
[~,sortidxct] = sort(top1allct,'descend');

top1allct = top1allct(rsfcdiscphewasoutput.sortidx);
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,top1allct'); 



%% Menhattan plot colored
top1allcomb = prctile(combined,99);
[~,sortidxcomb] = sort(top1allcomb,'descend');
figure; hold on;
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'vectorcomb','ascend');
for s = 1:size(rsfcbx_allefffectstable,1)
    k = scatter(ones(size(combined,1),1)+s-1,combined(:,sortidxcomb(s)),'jitter','on','jitteramount',0.3);
    k.Marker = '.'; k.SizeData = 30;
    if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
        k.MarkerFaceColor = [116 196 118]./255;
        k.MarkerEdgeColor = [116 196 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
        k.MarkerFaceColor = [166 206 227]./255;
        k.MarkerEdgeColor = [166 206 227]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
        k.MarkerFaceColor = [10 107 10]./255;
        k.MarkerEdgeColor = [10 107 10]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
        k.MarkerFaceColor = [158 154 200]./255;
        k.MarkerEdgeColor = [158 154 200]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
        k.MarkerFaceColor = [227 26 28]./255;
        k.MarkerEdgeColor = [227 26 28]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
        k.MarkerFaceColor = [0 0 118]./255;
        k.MarkerEdgeColor = [0 0 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
        k.MarkerFaceColor = [255 255 127]./255;
        k.MarkerEdgeColor = [255 255 127]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
        k.MarkerFaceColor = [251 154 153]./255;
        k.MarkerEdgeColor = [251 154 153]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
        k.MarkerFaceColor = [106 61 154]./255;
        k.MarkerEdgeColor = [106 61 154]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
        k.MarkerFaceColor = [253 191 111]./255;
        k.MarkerEdgeColor = [253 191 111]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k.MarkerFaceColor = [31 120 180]./255;
        k.MarkerEdgeColor = [31 120 180]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
        k.MarkerFaceColor = [0 128 128]./255;
        k.MarkerEdgeColor = [0 128 128]./255;
   end
   j = scatter(s,top1allcomb(sortidxcomb(s)),'filled');
   j.Marker = 'd'; j.MarkerEdgeColor = [0 0 0]; j.MarkerFaceColor = [0 0 0];
end
set(gca,'FontSize',8,'LineWidth',2,'FontName','Arial')
ylim([-.01 .26]); %xticks([1:length(varnames)]); xticklabels(varnames(sortidx)); xtickangle(90);


%% Menhattan plot cort thickness only
top1allct = prctile(allbrbxphewas,99);
[~,sortidxct] = sort(top1allct,'descend');
figure; hold on;
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'vectorct','ascend');
for s = 1:size(rsfcbx_allefffectstable,1)
    k = scatter(ones(size(allbrbxphewas,1),1)+s-1,allbrbxphewas(:,sortidxct(s)),'jitter','on','jitteramount',0.3);
    k.Marker = '.'; k.SizeData = 30;
    if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
        k.MarkerFaceColor = [116 196 118]./255;
        k.MarkerEdgeColor = [116 196 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
        k.MarkerFaceColor = [166 206 227]./255;
        k.MarkerEdgeColor = [166 206 227]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
        k.MarkerFaceColor = [10 107 10]./255;
        k.MarkerEdgeColor = [10 107 10]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
        k.MarkerFaceColor = [158 154 200]./255;
        k.MarkerEdgeColor = [158 154 200]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
        k.MarkerFaceColor = [227 26 28]./255;
        k.MarkerEdgeColor = [227 26 28]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
        k.MarkerFaceColor = [0 0 118]./255;
        k.MarkerEdgeColor = [0 0 118]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
        k.MarkerFaceColor = [255 255 127]./255;
        k.MarkerEdgeColor = [255 255 127]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
        k.MarkerFaceColor = [251 154 153]./255;
        k.MarkerEdgeColor = [251 154 153]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
        k.MarkerFaceColor = [106 61 154]./255;
        k.MarkerEdgeColor = [106 61 154]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
        k.MarkerFaceColor = [253 191 111]./255;
        k.MarkerEdgeColor = [253 191 111]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k.MarkerFaceColor = [31 120 180]./255;
        k.MarkerEdgeColor = [31 120 180]./255;
    elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
        k.MarkerFaceColor = [0 128 128]./255;
        k.MarkerEdgeColor = [0 128 128]./255;
   end
   j = scatter(s,top1allct(sortidxct(s)),'filled');
   j.Marker = 'd'; j.MarkerEdgeColor = [0 0 0]; j.MarkerFaceColor = [0 0 0];
end
set(gca,'FontSize',8,'LineWidth',2,'FontName','Arial')
ylim([0 .26]); %xticks([1:length(varnames)]); xticklabels(varnames(sortidx)); xtickangle(90);

%%
allcorrs = [ctdiscphewas.allcorrs(:,rsfcdiscphewasoutput.idx) ctdiscphewas.allcorrs2(:,rsfcdiscphewasoutput.idx2) ...
    ctdiscphewas.allcorrs3(:,rsfcdiscphewasoutput.idx3) , ctdiscphewas.allcorrs4(:,rsfcdiscphewasoutput.idx4) ...
    ctdiscphewas.allcorrs5(:,rsfcdiscphewasoutput.idx5)];
allbrbxphewas_noabs = allcorrs;
ctdisc_brainmapcorrs = corrcoef(allbrbxphewas_noabs);
ctdisc_brainmapcorrs_abs = abs(ctdisc_brainmapcorrs);
cmap = inferno(256);
figure;imagesc(ctdisc_brainmapcorrs_abs(rsfcdiscphewasoutput.sortidx,rsfcdiscphewasoutput.sortidx));colorbar;colormap(cmap);
ctdisc_brainmapcorrs_abs_sorted = ctdisc_brainmapcorrs_abs(rsfcdiscphewasoutput.sortidx,rsfcdiscphewasoutput.sortidx);

%% sort brain maps by cateogry 

rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Effect_Size','descend');
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Category','ascend');
ctdisc_brainmapcorrs_abs_sorted_bycategory = ctdisc_brainmapcorrs_abs_sorted(rsfcbx_allefffectstable.vector,rsfcbx_allefffectstable.vector);
% resort by indices
idx1 = find(strcmpi('Socioeconomic' , rsfcbx_allefffectstable.Category)==1);
idx2 = find(strcmpi('Screen Time' , rsfcbx_allefffectstable.Category)==1);
idx3 = find(strcmpi('Cognition' , rsfcbx_allefffectstable.Category)==1);

idx4 = find(strcmpi('Demographics' , rsfcbx_allefffectstable.Category)==1);
idx5 = find(strcmpi('Culture/Environment' , rsfcbx_allefffectstable.Category)==1);
idx6 = find(strcmpi('Physical Health' , rsfcbx_allefffectstable.Category)==1);

idx7 = find(strcmpi('Mental Health' , rsfcbx_allefffectstable.Category)==1);
idx8 = find(strcmpi('Social Adjustment' , rsfcbx_allefffectstable.Category)==1);
idx9 = find(strcmpi('Substance Use' , rsfcbx_allefffectstable.Category)==1);

idx10 = find(strcmpi('Parenting' , rsfcbx_allefffectstable.Category)==1);
idx11 = find(strcmpi('Personality' , rsfcbx_allefffectstable.Category)==1);
idx12 = find(strcmpi('Medical History' , rsfcbx_allefffectstable.Category)==1);

cateffsort = [idx1 ; idx2 ; idx3 ; idx4 ; idx5 ; idx6 ; idx7 ; idx8 ; idx9 ; idx10; idx11 ; idx12];

figure;imagesc(ctdisc_brainmapcorrs_abs_sorted_bycategory(cateffsort,cateffsort));colormap(cmap);

%% mimic fig 2c for ct 
topord = top1allct(sortidxct);
% rsfcdisc_brainmapcorrs_abs_sortedbycat = rsfcdisc_brainmapcorrs_abs_sorted(rsfcbx_allefffectstable.vector,rsfcbx_allefffectstable.vector);
%topord = topord(rsfcbx_allefffectstable.vector);
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'vectorct','ascend');
figure;hold on; 
for s = 1:size(rsfcbx_allefffectstable,1)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(ctdisc_brainmapcorrs_abs_sorted(s,7),topord(s));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')


% life factor correlation with ses - effect size in brain
figure;hold on; 
for s = 1:size(rsfcbx_allefffectstable,1)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(rsfcbx_allefffectstable.lf_w_ses(s),rsfcbx_allefffectstable.Effect_size_ct(s));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

%% Fig 2a supplement for cortical thickness

% load data 
load('pmtablectdisc.mat')
rsfcpmt=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_disc.txt');
load('pmtablectrep.mat')
rsfcpmtrep=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_rep.txt');

pmtablerep = pmtablectrep;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
variables = [pmtable.Age pmtable.Sex cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend), pmtable.FamilyIncome pmtable.sleepdisturb1_p_behdisc cellfun(@str2double,pmtable.demo_prnt_marital_v2b)];
variables(nanidx,:) = [];
% rsfcpmt = rsfcdiscthree;
rsfcpmt(nanidx,:) = [];
pmtable(nanidx,:) = [];
variables(:,6) = variables(:,6) + variables(:,7); variables(:,7) = [];
variables = zscore(variables);
variables = array2table(variables);
variables.Properties.VariableNames{1} = 'Age';
variables.Properties.VariableNames{2} = 'Sex';
variables.Properties.VariableNames{3} = 'ses';
variables.Properties.VariableNames{4} = 'iq';
variables.Properties.VariableNames{5} = 'p';
variables.Properties.VariableNames{6} = 'screentime';
variables.Properties.VariableNames{7} = 'fi';
variables.Properties.VariableNames{8} = 'sd';
variables.Properties.VariableNames{9} = 'marital';
variables = addvars(variables,pmtable.abcd_lt01_site_id_l);
variables.Properties.VariableNames{10} = 'site';


% replication set 
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);
variablesrep = [pmtablerep.Age pmtablerep.Sex cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend), pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)];
variablesrep(nanidxrep,:) = [];
% rsfcpmt = rsfcdiscthree;
%pmtablerep(nanidxrep,:) = [];
variablesrep(:,6) = variablesrep(:,6) + variablesrep(:,7); variablesrep(:,7) = [];
variablesrep = zscore(variablesrep);
variablesrep = array2table(variablesrep);
variablesrep.Properties.VariableNames{1} = 'Age';
variablesrep.Properties.VariableNames{2} = 'Sex';
variablesrep.Properties.VariableNames{3} = 'ses';
variablesrep.Properties.VariableNames{4} = 'iq';
variablesrep.Properties.VariableNames{5} = 'p';
variablesrep.Properties.VariableNames{6} = 'screentime';
variablesrep.Properties.VariableNames{7} = 'fi';
variablesrep.Properties.VariableNames{8} = 'sd';
variablesrep.Properties.VariableNames{9} = 'marital';
variablesrep = addvars(variablesrep,pmtablerep.abcd_lt01_site_id_l(~nanidxrep));
variablesrep.Properties.VariableNames{10} = 'site';

rsfcpmt = table2array(rsfcpmt);
rsfcpmtrep = table2array(rsfcpmtrep);

for i = 1:size(rsfcpmt,2)
   ses(i,1) = abs(corr(rsfcpmt(:,i),variables.ses)); 
   sleep(i,1) = abs(corr(rsfcpmt(:,i),variables.sd)); 
   screens(i,1) = abs(corr(rsfcpmt(:,i),variables.screentime)); 
   iq(i,1) = abs(corr(rsfcpmt(:,i),variables.iq)); 
   p(i,1) = abs(corr(rsfcpmt(:,i),variables.p)); 
end


% norm each to their own max
sesnorm = (ses - min(ses)) ./ range(ses);
sleepnorm = (sleep - min(sleep)) ./ range(sleep);
screennorm = (screens - min(screens)) ./ range(screens);
iqnorm = (iq - min(iq)) ./ range(iq);
pnorm = (p - min(p)) ./ range(p);


% Figures - parcels
addpath('/data/cn/data1/scripts/CIFTI_RELATED/Resources/read_write_cifti/fileio/')
rmpath('/data/cn/data1/scripts/CIFTI_RELATED/Resources/DEPRECATED/cifti-matlab-master/')

tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
out = zeros(size(tf.data,1),5);
out(:,1) = sesnorm;
out(:,2) = screennorm;
out(:,3) = sleepnorm;
out(:,4) = iqnorm;
out(:,5) = pnorm;
tf.data(1:59412,1:5) = out;
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/ctsupp_fig2a_brainmaps',tf)
system(['/data/nil-bluearc/GMT/Scott/workbench/bin_rh_linux64/wb_command -cifti-smoothing /data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/ctsupp_fig2a_brainmaps.dtseries.nii 2.55 0 COLUMN /data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/ctsupp_fig2a_brainmaps_smooth2.55.dtseries.nii -left-surface /data/cn/data1/scripts/CIFTI_RELATED/Resources/Conte69_atlas-v2.LR.32k_fs_LR.wb/Conte69.L.inflated.32k_fs_LR.surf.gii -right-surface /data/cn/data1/scripts/CIFTI_RELATED/Resources/Conte69_atlas-v2.LR.32k_fs_LR.wb/Conte69.R.inflated.32k_fs_LR.surf.gii']);

%% Figure 3b for cortical thickness 

% load data 
load('pmtablectdisc.mat')
rsfcpmt=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_disc.txt');
load('pmtablectrep.mat')
rsfcpmtrep=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_rep.txt');

pmtablerep = pmtablectrep;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
variables = [pmtable.Age pmtable.Sex cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend), pmtable.FamilyIncome pmtable.sleepdisturb1_p_behdisc cellfun(@str2double,pmtable.demo_prnt_marital_v2b)];
variables(nanidx,:) = [];
% rsfcpmt = rsfcdiscthree;
rsfcpmt(nanidx,:) = [];
pmtable(nanidx,:) = [];
variables(:,6) = variables(:,6) + variables(:,7); variables(:,7) = [];
variables = zscore(variables);
variables = array2table(variables);
variables.Properties.VariableNames{1} = 'Age';
variables.Properties.VariableNames{2} = 'Sex';
variables.Properties.VariableNames{3} = 'ses';
variables.Properties.VariableNames{4} = 'iq';
variables.Properties.VariableNames{5} = 'p';
variables.Properties.VariableNames{6} = 'screentime';
variables.Properties.VariableNames{7} = 'fi';
variables.Properties.VariableNames{8} = 'sd';
variables.Properties.VariableNames{9} = 'marital';
variables = addvars(variables,pmtable.abcd_lt01_site_id_l);
variables.Properties.VariableNames{10} = 'site';


% replication set 
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , cellfun(@str2double,pmtablerep.sleepdisturb1_p) , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);
variablesrep = [pmtablerep.Age pmtablerep.Sex cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend), pmtablerep.FamilyIncome , cellfun(@str2double,pmtablerep.sleepdisturb1_p) , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)];
variablesrep(nanidxrep,:) = [];
% rsfcpmt = rsfcdiscthree;
%pmtablerep(nanidxrep,:) = [];
variablesrep(:,6) = variablesrep(:,6) + variablesrep(:,7); variablesrep(:,7) = [];
variablesrep = zscore(variablesrep);
variablesrep = array2table(variablesrep);
variablesrep.Properties.VariableNames{1} = 'Age';
variablesrep.Properties.VariableNames{2} = 'Sex';
variablesrep.Properties.VariableNames{3} = 'ses';
variablesrep.Properties.VariableNames{4} = 'iq';
variablesrep.Properties.VariableNames{5} = 'p';
variablesrep.Properties.VariableNames{6} = 'screentime';
variablesrep.Properties.VariableNames{7} = 'fi';
variablesrep.Properties.VariableNames{8} = 'sd';
variablesrep.Properties.VariableNames{9} = 'marital';
variablesrep = addvars(variablesrep,pmtablerep.abcd_lt01_site_id_l(~nanidxrep));
variablesrep.Properties.VariableNames{10} = 'site';

rsfcpmt = table2array(rsfcpmt);
rsfcpmtrep = table2array(rsfcpmtrep);

% all edges
seststat_adj = zeros(size(rsfcpmt,2),1); 
iqtstat_adj = zeros(size(rsfcpmt,2),1);
sttstat_adj = zeros(size(rsfcpmt,2),1);
sdtstat_adj = zeros(size(rsfcpmt,2),1);

seststat_unadj = zeros(size(rsfcpmt,2),1); 
iqtstat_unadj = zeros(size(rsfcpmt,2),1); 
sttstat_unadj = zeros(size(rsfcpmt,2),1); 
sdtstat_unadj = zeros(size(rsfcpmt,2),1); 

% z=parpool(5);
for i = 1:size(rsfcpmt,2)
    disp([' On edge ' num2str(i)])
    this = zscore(rsfcpmt(:,i));
    subsdisc = addvars(variables,this);
    mdl = fitlme(subsdisc,'this ~ Age + Sex + iq + ses + screentime + sd + (1|site)');
    seststat_adj(i,1) = table2array(mdl.Coefficients(4,2));
    iqtstat_adj(i,1) = table2array(mdl.Coefficients(5,2));
    sttstat_adj(i,1) = table2array(mdl.Coefficients(6,2));
    sdtstat_adj(i,1) = table2array(mdl.Coefficients(7,2));
    
    mdl = fitlme(subsdisc,'this ~ Age + Sex + ses + (1|site)');
    seststat_unadj(i,1) = table2array(mdl.Coefficients(4,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + iq + (1|site)');
    iqtstat_unadj(i,1) = table2array(mdl.Coefficients(4,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + screentime + (1|site)');
    sttstat_unadj(i,1) = table2array(mdl.Coefficients(4,2));
    mdl = fitlme(subsdisc,'this ~ Age + Sex + sd +(1|site)');
    sdtstat_unadj(i,1) = table2array(mdl.Coefficients(4,2));
end
% delete(z)

[d drand] = compare_effectsizes(seststat_adj , iqtstat_adj);

rsfcpcorrs = zeros(59412,1);
for i = 1:length(rsfcpcorrs)
    ctpcorrs(i,1) = corr(rsfcpmt(:,i),variables.p);
end
[d drand] = compare_effectsizes(ctpcorrs , seststat_adj);

% plot supplemental fig 3b


%% generalizability to HCP 

load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmt.mat')
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/pmtable.mat')
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmtrep.mat');
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/pmtablerep.mat');

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);

pmtable(nanidx,:) = [];

% read in HCP
hcpfc = readtable('/data/nil-bluearc/GMT/Scott/HCP/matters_arising/netmats_pearson.txt','ReadVariableNames',0);
hcpfc = table2array(hcpfc);
hcpbeh = readtable('/data/nil-bluearc/GMT/Scott/HCP/matters_arising/behavioraltable.txt');

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected ];
[A B r U V] = canoncorr(x,y); r;

rsfcrep = hcpfc;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% train on high abcd, test on hcp 
rsfcpmth = rsfcpmt(variables.ses>.75,:);

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

YHdisc = Y(variables.ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); 

rsfcrep = hcpfc;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr)

% combine discovery and replication in abcd and repeat
Xabcd = [rsfcpmt ; rsfcpmtrep];
[coeffs,brainscores,~,~,explained,mu] = pca(Xabcd);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected ];
yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
    pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
    pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
    pmtablerep.nihtbx_reading_uncorrected ];
Yabcd = [y ; yrep];
[A B r U V] = canoncorr(x,Yabcd); r;

rsfcrep = hcpfc;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% combine discovery and replication in abcd and repeat - high ses only in
% abcd
Xabcd = [rsfcpmt ; rsfcpmtrep];
abcdses = zscore([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]);
Xabcd = Xabcd(abcdses > 0.75,:);
[coeffs,brainscores,~,~,explained,mu] = pca(Xabcd);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);

Yabcd = [y ; yrep];
Yabcd = Yabcd(abcdses>0.75,:);
[A B r U V] = canoncorr(x,Yabcd); r;

rsfcrep = hcpfc;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% varying ses threshold for training on abcd - test on full hcp 
Xabcd = [rsfcpmt ; rsfcpmtrep];
abcdses = zscore([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]);
%abcdses = -1.*zscore([pmtable.ADIPercentile ; pmtablerep.ADIPercentile]);
abcdp = zscore([pmtable.TotalProblems ; pmtablerep.TotalProblems]);

y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected ];
yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
    pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
    pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
    pmtablerep.nihtbx_reading_uncorrected ];
Yabcd = [y ; yrep];

samplingvectorses = flipud([-4:.05:.80]');
% samplingvectorses = flipud([-2.58:.035:.80]'); % ADI raw
samplingvectorp = [-.56:0.04:3.3]'; % for p
hcpoosr_ses = zeros(500,97);
hcpoosr_p = zeros(500,97);
rsfcrep = hcpfc;
yun = zscore([hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj ...
    hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj]);

z=parpool(8);
for t = 2:length(samplingvectorses)
    disp(['On interation ' num2str(t)])
%     thishcpoosr_ses = zeros(500,1);
    thishcpoosr_p = zeros(500,1);
%     thrses = samplingvectorses(t);
    thrp = samplingvectorp(t);
     % define abcd sample
%     thisxses = Xabcd(abcdses > thrses,:);
%     thisyses = Yabcd(abcdses > thrses,:);
%     
    thisxp = Xabcd(abcdp < thrp,:);
    thisyp = Yabcd(abcdp < thrp,:);

    for i = 1:500
        
%         % SES 
%         thisidx=datasample(1:size(thisxses,1),877)';
% %         thisidx=randperm(size(thisxses,1))';
% %         thisidx = thisidx(1:877);
%         % subsample 1000 individuals from distribution
%         thisxidx = zscore(thisxses(thisidx,:));
%         thisyidx = zscore(thisyses(thisidx,:));
% 
%         [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
%         rsfc_varexplained = cumsum(explained);
%         % Using components accounting for 20% variance exaplained for prediction
%         fn = 20;
%         fn = find(rsfc_varexplained>fn,1);
%         x = brainscores(:,1:fn);
%         [A B r U V] = canoncorr(x,thisyidx); 
% 
%         
%         brainscores_rep = (rsfcrep - mu)*coeffs;
%         xun = brainscores_rep(:,1:fn);
%         thishcpoosr_ses(i,1) = corr(xun*A(:,1),yun*B(:,1));
        
        % P factor foil
        thisidx=datasample(1:size(thisxp,1),877)';
%          thisidx=randperm(size(thisxp,1))';
%          thisidx = thisidx(1:877);
       
        % subsample 1000 individuals from distribution
        thisxidx = zscore(thisxp(thisidx,:));
        thisyidx = zscore(thisyp(thisidx,:));

        [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
        rsfc_varexplained = cumsum(explained);
        % Using components accounting for 20% variance exaplained for prediction
        fn = 20;
        fn = find(rsfc_varexplained>fn,1);
        x = brainscores(:,1:fn);
        [A B r U V] = canoncorr(x,thisyidx); 

        
        brainscores_rep = (rsfcrep - mu)*coeffs;
        xun = brainscores_rep(:,1:fn);
        thishcpoosr_p(i,1) = corr(xun*A(:,1),yun*B(:,1));
        
        
    end
    %disp([' mean on bin ' num2str(t) ' is ' num2str(mean(hcpoosr,1))])
    hcpoosr_p(:,t) = thishcpoosr_p;
    %hcpoosr_ses(:,t) = thishcpoosr_ses;
end
delete(z)
hcpoosr = [hcpoosr ; hcpoosrb];
mhcpoosr = mean(hcpoosr,1);
figure;plot(mhcpoosr);

%% Train on HCP, test on ABCD
[coeffs,brainscores,~,~,explained,mu] = pca(hcpfc);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [hcpbeh.CardSort_AgeAdj hcpbeh.Flanker_AgeAdj hcpbeh.ListSort_AgeAdj hcpbeh.ProcSpeed_AgeAdj hcpbeh.PicSeq_AgeAdj hcpbeh.PicVocab_AgeAdj hcpbeh.ReadEng_AgeAdj];

[A B r U V] = canoncorr(x,y); r;

rsfcrep = [rsfcpmt ; rsfcpmtrep];
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected ];
yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
    pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
    pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
    pmtablerep.nihtbx_reading_uncorrected ];
yun = [y ; yrep];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% test only low ses
abcdses = zscore([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]);
xun = brainscores_rep(abcdses<-.25,1:fn);
yun = [y ; yrep];
yun = yun(abcdses<-.25,:);
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% test only high ses
abcdses = zscore([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]);
xun = brainscores_rep(abcdses>.75,1:fn);
yun = [y ; yrep];
yun = yun(abcdses>.75,:);
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

%% combine discovery and replication in abcd (train) and test UKB
load('rsfcpmt.mat')
load('pmtable.mat')
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmtrep.mat');
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/pmtablerep.mat');

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);

pmtable(nanidx,:) = [];

% Read in UKB 
load('UKB_rsfcmatrices_32572.mat'); ukb = mats; clear mats
load('behavioraltable_FIQ_32572.mat');
ukbiq = subjecttable.FIQ;

Xabcd = [rsfcpmt ; rsfcpmtrep];
[coeffs,brainscores,~,~,explained,mu] = pca(Xabcd);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [ pmtable.nihtbx_totalcomp_uncorrected ];
yrep = [ pmtablerep.nihtbx_totalcomp_uncorrected ];
Yabcd = [y ; yrep];
[A B r U V] = canoncorr(x,Yabcd); r;

rsfcrep = ukb;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = ukbiq;
oosr = corr(xun*A(:,1),yun*B(:,1));
oosr = diag(oosr);

% combine discovery and replication in abcd and repeat - high ses only in
% abcd
Xabcd = [rsfcpmt ; rsfcpmtrep];
abcdses = zscore([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) ; cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat)]);
Xabcd = Xabcd(abcdses > 0.75,:);
[coeffs,brainscores,~,~,explained,mu] = pca(Xabcd);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);

Yabcd = [y ; yrep];
Yabcd = Yabcd(abcdses>0.75,:);
[A B r U V] = canoncorr(x,Yabcd); r;

rsfcrep = ukb;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = ukbiq;
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);


%% cortical thickness reproducibility & generalizability of ct w iq
load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmt_ctdisc.mat');
rsfcpmt = rsfcdisc; clear rsfcdisc
pmtablerep = load('pmtablectrep.mat');
rsfcpmtrep=load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/rsfcpmt_ctrep.mat');
pmtablerep = pmtablerep.pmtable;
rsfcpmtrep = rsfcpmtrep.rsfcdisc;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility ]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility]),2);
nanidxrep = logical(nanidxrep);

rsfcpmt(nanidx == 1,:) = [];
rsfcpmtrep(nanidxrep == 1,:) = [];
pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

ses = zscore(cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat));
sesrep = zscore(cellfun(@str2double , pmtablerep.reshist_addr1_coi_z_se_nat));
income = zscore(pmtable.FamilyIncome);
incomerep = zscore(pmtablerep.FamilyIncome);

% Fig 5a for cortical thickness
% subsample of n = 565
for i = 1:100
    disp(['On interation ' num2str(i)])
    idx = randperm(size(rsfcpmt,1));
    [coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt(idx(1:565),:));
    rsfc_varexplained = cumsum(explained);
    % Using components accounting for 20% variance exaplained for prediction
    fn = 20;
    fn = find(rsfc_varexplained>fn,1);
    x = brainscores(:,1:fn);
    y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
        pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
        pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
        pmtable.nihtbx_reading_uncorrected ];
    y = y(idx(1:565),:);
    [A B r U V] = canoncorr(x,y); 

    % replication
    % Apply out of sample 
    rsfcrep = rsfcpmtrep;
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    yun = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
        pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
        pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
        pmtablerep.nihtbx_reading_uncorrected ];
    thisoosr = corr(xun*A,yun*B);
    thisoosr = diag(thisoosr);
    meanoosr(i,1) = thisoosr(1);
end

% train on discovery, test on replication, controlling test set for ses, do
% 100x 
abcdX = [rsfcpmt ; rsfcpmtrep];
abcdY = [pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
    pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
    pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ... 
    pmtable.nihtbx_reading_uncorrected  ; 
    pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
    pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
    pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ... 
    pmtablerep.nihtbx_reading_uncorrected];
abcdses = [ses ; sesrep];

oosr_unadj = []; oosr_adj = [];
for i = 1:100
    idx = randperm(size(abcdX,1));
    idxd = idx(1:2174);
    idxr = idx(2175:end);
    sesr = abcdses(idx(2175:end),:);
    
    [coeffs,brainscores,~,~,explained,mu] = pca(abcdX(idxd,:));
    rsfc_varexplained = cumsum(explained);
    % Using components accounting for 20% variance exaplained for prediction
    fn = 20;
    fn = find(rsfc_varexplained>fn,1);
    x = brainscores(:,1:fn);
    y = abcdY(idxd,:);
    [A B r U V] = canoncorr(x,y); r;
    
    % Apply out of sample 
    rsfcrep = abcdX(idxr,:);
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    Yrep = abcdY(idxr,:);
    [oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
    oosr_unadj(:,i) = diag(oosr);
    
    % Apply out of sample , after adjusting for ses 
    rsfcrep = abcdX(idxr,:);
    brainscores_rep = (rsfcrep - mu)*coeffs;
    xun = brainscores_rep(:,1:fn);
    for j = 1:size(abcdY,2)
        [~,~,resids(:,j)] = regress(abcdY(idxr,j) , [ones(size(rsfcrep,1),1) sesr]);
    end
    Yrep = resids;
    [oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
    oosr_adj(i,1) = diag(oosr);


end

oosrboot = [oosr_unadj' , oosr_adj];
figure; hold on 
for k = 1 : size(oosrboot,1)
    h=plot([1 2], [oosrboot(k,1), oosrboot(k,2)]);
    h.Color = [.8 .8 .8];
    h.LineWidth = 1.5;
    i = scatter(1,oosrboot(k,1));
    i.SizeData = 100;
    i.MarkerEdgeColor = [.5 .5 .5];
    j = scatter(2,oosrboot(k,2));
    j.SizeData = 100;
    j.MarkerEdgeColor = [116 196 118]./255;
end
xlim([.9 2.1])
h=plot([1 2], [.27, .19]);
h.Color = [.2 .2 .2];
h.LineWidth = 1.5;
i = scatter(1,.27);
i.SizeData = 200;
i.MarkerEdgeColor = [0 0 0];
i.Marker = 'd';
i.MarkerFaceColor = [0 0 0];
j = scatter(2,.19);
j.SizeData = 200;
j.MarkerEdgeColor = [0 104 0]./255;
j.Marker = 'd';
j.MarkerFaceColor = [0 104 0]./255;
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial','xtick',[])


% train high ses, test full replication 
rsfcpmth = rsfcpmt(ses>.75,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

Yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];

YHdisc = Y(ses>.75,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); rinsamp

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)

% plot - if you remove outlier corr is still 0.3971
figure;ins=scatter(zscore(xinsamp(:,1)),zscore(yinsamp(:,1))); ij = lsline;
ins.Marker = '.'; ins.SizeData = 200; 
ins.MarkerFaceColor = [116 198 118]./255;
ins.MarkerEdgeColor = [116 198 118]./255;
ins.MarkerFaceAlpha = .4;
ij.LineWidth = 2.5; ij.Color = 'k';
ylim([-5 3]);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
figure;
oos=scatter(zscore(xun*A(:,1)),zscore(Yrep*B(:,1))); oj = lsline;
oos.Marker = '.'; oos.SizeData = 200; 
oos.MarkerFaceColor = [0 109 44]./255;
oos.MarkerEdgeColor = [0 109 44]./255;
oos.MarkerFaceAlpha = .4;
oj.LineWidth = 2.5; oj.Color = 'k';
ylim([-5 3]);
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

%train ct with ses on discovery, test on replication
[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = 20;
fn = find(rsfc_varexplained>fn,1);
x = brainscores(:,1:fn);
y = [ ses income ];
[A B r U V] = canoncorr(x,y); r

% replication
% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);
yun = [ sesrep incomerep ];
oosr = corr(xun*A,yun*B);
oosr = diag(oosr);

% train ct with ses on high iq, test on full 
iq = zscore(pmtable.nihtbx_totalcomp_uncorrected);
rsfcpmth = rsfcpmt(iq>.5,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ ses , income];

Yrep = [ sesrep , incomerep ];
YHdisc = Y(iq>.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmth);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YHdisc); rinsamp

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)


% train ct with ses on low iq on discovery, test on full 
iq = zscore(pmtable.nihtbx_totalcomp_uncorrected);
rsfcpmtl = rsfcpmt(iq<-.5,:);
rsfcpmtrep = rsfcpmtrep;

Y = [ ses , income];

Yrep = [ sesrep , incomerep ];
YLdisc = Y(iq<-.5,:);

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmtl);
rsfc_varexplained = cumsum(explained);
% Using components accounting for 20% variance exaplained for prediction
fn = find(rsfc_varexplained>20,1);
X = brainscores(:,1:fn);
[A, B, rinsamp, xinsamp , yinsamp] = canoncorr(X,YLdisc); rinsamp

% Apply out of sample 
rsfcrep = rsfcpmtrep;
brainscores_rep = (rsfcrep - mu)*coeffs;
xun = brainscores_rep(:,1:fn);

[oosr pval] = corr(xun*A(:,1),Yrep*B(:,1));
oosr = diag(oosr)



%% Fig 3c for cortical thickness supplement 

load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/pmtablectdisc.mat')
% load('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/pmtablectrep.mat')
rsfcpmt=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_disc.txt');
% rsfcpmtrep=table2array(readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_rep.txt'));

% pmtablerep = pmtablectrep;

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
% nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , cellfun(@str2double,pmtablerep.sleepdisturb1_p) , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
% nanidxrep = logical(nanidxrep);


pmtable(nanidx==1,:) = [];
% rsfcpmtrep(nanidxrep==1,:) = [];
% pmtablerep(nanidxrep==1,:) = [];

varstoinc = [
    find(strcmpi('TotalCogAbility',pmtable.Properties.VariableNames) == 1) , ...
    find(strcmpi('FamilyIncome',pmtable.Properties.VariableNames) == 1) , ... 
    find(strcmpi('Age',pmtable.Properties.VariableNames) == 1) , ... 
    find(strcmpi('Sex',pmtable.Properties.VariableNames) == 1) , ... 
    find(strcmpi('FD',pmtable.Properties.VariableNames) == 1) ];
pmtable.reshist_addr1_coi_z_se_nat = cellfun( @str2double, pmtable.reshist_addr1_coi_z_se_nat);
func = @zscore;
test = varfun(func,pmtable,'InputVariables',varstoinc);
pmtable = [pmtable test];

sescoeffs_adj = zeros(size(rsfcpmt,2),1); 
cogcoeffs_adj = zeros(size(rsfcpmt,2),1);
cogcoeffs_unadj = zeros(size(rsfcpmt,2),1);
sescoeffs_unadj = zeros(size(rsfcpmt,2),1);
seststat_adj = zeros(size(rsfcpmt,2),1); 
cogtstat_adj = zeros(size(rsfcpmt,2),1);
cogtstat_unadj = zeros(size(rsfcpmt,2),1);
seststat_unadj = zeros(size(rsfcpmt,2),1);
for i = 1:size(rsfcpmt,2)
    disp(['On interation ' num2str(i)])
    this = zscore(rsfcpmt(:,i));
    subsdisc = addvars(pmtable,this);
    mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_TotalCogAbility + reshist_addr1_coi_z_se_nat + zscore_FamilyIncome + (1|abcd_lt01_site_id_l)');
%     sescoeffs_adj(i,1) = table2array(mdl.Coefficients(2,2));
    cogcoeffs_adj(i,1) = table2array(mdl.Coefficients(3,2));
%     seststat_adj(i,1) = table2array(mdl.Coefficients(2,4));
    rsfccogtstat_adj(i,1) = table2array(mdl.Coefficients(3,4));
    mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_TotalCogAbility +  (1|abcd_lt01_site_id_l)');
    cogcoeffs_unadj(i,1) = table2array(mdl.Coefficients(2,2));
    cogtstat_unadj(i,1) = table2array(mdl.Coefficients(2,4));
%     mdl = fitlme(subsdisc,'this ~ zscore_Age + zscore_Sex + zscore_sespc1 + zscore_FD + (1|abcd_lt01_site_id_l)');
%     rsfcsescoeffs_unadj(i,1) = table2array(mdl.Coefficients(5,2));
%     rsfcseststat_unadj(i,1) = table2array(mdl.Coefficients(5,4));
end
corr(rsfcseststat_unadj,rsfccogtstat_unadj)


% Make scatter plot of the association between t-statistics
figure; hold on;
h = histogram(abs([cogtstat_unadj]),edges);
h.FaceColor = [150 150 150]./255;
h.EdgeColor = [150 150 150]./255;
z = histogram(abs([cogtstat_adj]),edges);
z.FaceColor = [65 171 93]./255;
z.EdgeColor = [65 171 93]./255;
z.FaceAlpha = .4; z.EdgeAlpha = .4;
xlabel('|t-statistic|')
ylabel('Frequency')
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/ct_cog_sesadj_v_unadj','png')

% Percent of associations that decrease effect size 
unadj = abs(cogtstat_unadj(abs(cogtstat_unadj)>3));
adj = abs(cogtstat_adj(abs(cogtstat_unadj)>3));

difference = adj - unadj; 100.*(length(find(difference<0)) / length(difference))

% Percent of adj associations that are sig unadj & >= unadj value
sig_noinflation = 100*(length(find(difference>=0)) / length(difference));

% Percent of adj associations that are no longer sig 
nosig = 100*(length(find(adj<3)) / length(adj));

% Percent of adj associations that are sig unadj & < unadj value (the
% remainder)
sig_wasinflated = 100-sig_noinflation-nosig;

disp(['Percent significant and not inflated = ' num2str(sig_noinflation)])
disp(['Percent significant but were inflated = ' num2str(sig_wasinflated)])
disp(['Percent no longer significant = ' num2str(nosig)])

figure;h=scatter(unadj,adj-unadj);
j=lsline;
j.Color = [.9 .5 0]; 
j.LineWidth = 2.5;
h.Marker = '.';
h.SizeData = 200;
h.MarkerEdgeColor = 'k';
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')


%% Connectome based prediction model - RFSC 
addpath(genpath('/data/nil-bluearc/GMT/Scott/CPM-master/'))

load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);


pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

% cognition - unadjusted
allmats_cpm = [rsfcpmt ; rsfcpmtrep]';
iqdisc = pmtable.nihtbx_totalcomp_uncorrected;
iqrep = pmtablerep.nihtbx_totalcomp_uncorrected;
sesdisc = zscore(cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat));
sesrep = zscore(cellfun(@str2double , pmtablerep.reshist_addr1_coi_z_se_nat));
%faminc = [cellfun(@str2double , pmtable.demo_comb_income_v2b) ; cellfun(@str2double,pmtablerep.demo_comb_income_v2b)]';


% Train low, test full
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest;

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

figure;hold on ;
subplot(2,3,1)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));

% Train low, test low
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep(sesrep<-.25,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest(sesrep<-.25,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

%plot
subplot(2,3,2)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train low, test high
% low ses predicting high ses rsfc with iq 
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep(sesrep>0.75,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest(sesrep>0.75,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

% plot
subplot(2,3,3)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test full
xtrain = rsfcpmt(sesdisc>.75,:)';
xtest = rsfcpmtrep';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>.75,:);
ytest = ytest;

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);


% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);


subplot(2,3,4)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test low
xtrain = rsfcpmt(sesdisc>0.75,:)';
xtest = rsfcpmtrep(sesrep<-.25,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>0.75,:);
ytest = ytest(sesrep<-.25,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

%plot
subplot(2,3,5)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test high
xtrain = rsfcpmt(sesdisc>0.75,:)';
xtest = rsfcpmtrep(sesrep>0.75,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>0.75,:);
ytest = ytest(sesrep>0.75,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

% plot
subplot(2,3,6)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));



for i = 1:6
    subplot(2,3,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
    xlim([-5 5])
end



%% Connectome based prediction model - cortical thickness 
addpath(genpath('/data/nil-bluearc/GMT/Scott/CPM-master/'))

load('pmtablectdisc.mat')
rsfcpmt=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_disc.txt');
load('pmtablectrep.mat')
rsfcpmtrep=readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/ctvertices_rep.txt');

pmtablerep = pmtablectrep;

[coeffs,brainscores,~,~,explained,mu] = pca(rsfcpmt);

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility ]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility]),2);
nanidxrep = logical(nanidxrep);

rsfcpmt = table2array(rsfcpmt);
rsfcpmtrep = table2array(rsfcpmtrep);

rsfcpmt(nanidx == 1,:) = [];
rsfcpmtrep(nanidxrep == 1,:) = [];
pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

iqdisc = pmtable.nihtbx_totalcomp_uncorrected;
iqrep = pmtablerep.nihtbx_totalcomp_uncorrected;
sesdisc = zscore(cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat));
sesrep = zscore(cellfun(@str2double , pmtablerep.reshist_addr1_coi_z_se_nat));

% Train low, test full
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest;

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

figure;hold on ;
subplot(2,3,1)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));

% Train low, test low
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep(sesrep<-.25,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest(sesrep<-.25,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

%plot
subplot(2,3,2)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train low, test high
% low ses predicting high ses rsfc with iq 
xtrain = rsfcpmt(sesdisc<-.25,:)';
xtest = rsfcpmtrep(sesrep>0.75,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc<-.25,:);
ytest = ytest(sesrep>0.75,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

% plot
subplot(2,3,3)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test full
xtrain = rsfcpmt(sesdisc>.75,:)';
xtest = rsfcpmtrep';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>.75,:);
ytest = ytest;

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);


% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);


subplot(2,3,4)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test low
xtrain = rsfcpmt(sesdisc>0.75,:)';
xtest = rsfcpmtrep(sesrep<-.25,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>0.75,:);
ytest = ytest(sesrep<-.25,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

%plot
subplot(2,3,5)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));


% Train high, test high
xtrain = rsfcpmt(sesdisc>0.75,:)';
xtest = rsfcpmtrep(sesrep>0.75,:)';

ytrain = iqdisc;

ytest = iqrep;

ytrain = ytrain(sesdisc>0.75,:);
ytest = ytest(sesrep>0.75,:);

% Train Connectome-based Predictive Model
[r ,p , pmask, mdl] = cpm_train(xtrain,ytrain,0.01);

% Test Connectome-based Predictive Model
y_predict = cpm_test(xtest,mdl,pmask);

% plot
subplot(2,3,6)
h=scatter(zscore(y_predict'),zscore(ytest)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(y_predict'),zscore(ytest))));



for i = 1:6
    subplot(2,3,i)
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
    xlim([-5 5])
end



%% compare rsfc effect sizes using cohen's D + permuting edges

load('rsfcdiscphewasoutput.mat');
allcorrs = rsfcdiscphewasoutput.allcorrs; allcorrs2 = rsfcdiscphewasoutput.allcorrs2; allcorrs3 = rsfcdiscphewasoutput.allcorrs3; allcorrs4 = rsfcdiscphewasoutput.allcorrs4; allcorrs5 = rsfcdiscphewasoutput.allcorrs5;
idx = rsfcdiscphewasoutput.idx; idx2 = rsfcdiscphewasoutput.idx2; idx3 = rsfcdiscphewasoutput.idx3; idx4 = rsfcdiscphewasoutput.idx4; idx5 = rsfcdiscphewasoutput.idx5;
allidx = [idx ; idx2 ; idx3 ; idx4; idx5];
allbrbxphewas = [abs(allcorrs(:,idx)) abs(allcorrs2(:,idx2)) abs(allcorrs3(:,idx3)) abs(allcorrs4(:,idx4)) abs(allcorrs5(:,idx5))];
top1all = prctile(allbrbxphewas,99);
[~,sortidx] = sort(top1all,'descend');

% cohens D between ses and Iq
d = computeCohen_d(abs(allbrbxphewas(:,589)) , abs(allbrbxphewas(:,642)))

% do permutation 10,000x
alledges = [abs(allbrbxphewas(:,589)) ; abs(allbrbxphewas(:,642))];
for i = 1:10000
    disp(['On interation ' num2str(i)]);
    idx = randperm(110556);
    samp1 = alledges(idx(1:55278));
    samp2 = alledges(idx(55279:110556));
    drand(i,1) = computeCohen_d(samp1 , samp2);
end


%% Fig S5
load('rsfclmeoutput.mat')
figure; hold on
% ses undj
h = scatter(ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.sesunadjusted) ,'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [31 120 180]./255;
h.MarkerEdgeColor = [31 120 180]./255;
% sleep unadj
h = scatter(2+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.sdunadjusted),'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [10 107 10]./255;
h.MarkerEdgeColor = [10 107 10]./255;
% screen undj
h = scatter(4+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.stunadjusted) ,'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [0 0 118]./255;
h.MarkerEdgeColor = [0 0 118]./255;
% iq unadj
h = scatter(6+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.iqunadjusted),'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [116 196 118]./255;
h.MarkerEdgeColor = [10 107 10]./255;

% ses adj
h = scatter(10+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.sesadjusted) ,'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [31 120 180]./255;
h.MarkerEdgeColor = [31 120 180]./255;
% sleep adj
h = scatter(12+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.sdadjusted),'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [10 107 10]./255;
h.MarkerEdgeColor = [10 107 10]./255;
% screen adj
h = scatter(14+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.stadjusted) ,'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [0 0 118]./255;
h.MarkerEdgeColor = [0 0 118]./255;
% iq adj
h = scatter(16+ones(size(rsfclme.sesunadjusted,1),1) , abs(rsfclme.iqadjusted),'jitter','on');
h.SizeData = 60;
h.MarkerFaceAlpha = .05;
h.MarkerEdgeAlpha = .05;
h.MarkerFaceColor = [116 196 118]./255;
h.MarkerEdgeColor = [10 107 10]./255;


%% make new 2c and 2d (1st pc of brain maps & corrs to it)
load('rsfcdiscphewasoutput.mat')
load('rsfcbx_table_discpluscombatplusrep.mat')

allbrbxsorted = rsfcdiscphewasoutput.allbrbxphewas_noabs(:,rsfcdiscphewasoutput.sortidx');
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Effect_Size','descend');
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Category','ascend');

allbrbxsortedbycat = allbrbxsorted(:,rsfcbx_allefffectstable.vector);

% PCA
[coeffs,mapscores,~,~,explained,mu] = pca(allbrbxsortedbycat);
varexp = cumsum(explained);

% Correlate each brain map w pc1 brain map
for i = 1:size(allbrbxsortedbycat,2)
    pcmapcorrs(i,1) = corr(mapscores(:,2) , allbrbxsortedbycat(:,i));
end
pcmapcorrs_abs = abs(pcmapcorrs);

% Plot by each category & color accordingly
figure; hold on
alphalevel = .1;
for s = 1:size(allbrbxsortedbycat,2)
        k.Marker = 'o'; k.SizeData = 50;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k=scatter(1,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k=scatter(2,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k=scatter(3,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k=scatter(4,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k=scatter(5,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k=scatter(6,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k=scatter(7,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k=scatter(8,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k=scatter(9,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k=scatter(10,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k=scatter(11,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k=scatter(12,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
end

% Correlate each brain map w pc1 brain map
rsfcbx_allefffectstable = sortrows(rsfcbx_allefffectstable,'Effect_Size','descend');
for i = 1:size(allbrbxsorted,2)
    pcmapcorrs(i,1) = corr(mapscores(:,4) , allbrbxsorted(:,i));
end
pcmapcorrs_abs = abs(pcmapcorrs);
%[a,sidx] = sort(pc1mapcorrs_abs,'descend');
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,pcmapcorrs_abs);
[rsfcbx_allefffectstable,sidx] = sortrows(rsfcbx_allefffectstable,'pcmapcorrs_abs','descend');
pcmapcorrs_abs = pcmapcorrs_abs(sidx);

% Plot by each category & color accordingly
figure; hold on
alphalevel = .1;
for s = 1:size(rsfcbx_allefffectstable,1)
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k=scatter(s,pcmapcorrs_abs(s));
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerFaceAlpha = alphalevel;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
end


% Get mean and SEM of each category & plot
catinfo(1,1) = mean(pc1mapcorrs_abs(strcmpi('Socioeconomic',rsfcbx_allefffectstable.Category)));
catinfo(1,2) = (std(pc1mapcorrs_abs(strcmpi('Socioeconomic',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Socioeconomic',rsfcbx_allefffectstable.Category))));

catinfo(2,1) = mean(pc1mapcorrs_abs(strcmpi('Cognition',rsfcbx_allefffectstable.Category)));
catinfo(2,2) = (std(pc1mapcorrs_abs(strcmpi('Cognition',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Cognition',rsfcbx_allefffectstable.Category))));

catinfo(3,1) = mean(pc1mapcorrs_abs(strcmpi('Screen Time',rsfcbx_allefffectstable.Category)));
catinfo(3,2) = (std(pc1mapcorrs_abs(strcmpi('Screen Time',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Screen Time',rsfcbx_allefffectstable.Category))));

catinfo(4,1) = mean(pc1mapcorrs_abs(strcmpi('Physical Health',rsfcbx_allefffectstable.Category)));
catinfo(4,2) = (std(pc1mapcorrs_abs(strcmpi('Physical Health',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Physical Health',rsfcbx_allefffectstable.Category))));

catinfo(5,1) = mean(pc1mapcorrs_abs(strcmpi('Mental Health',rsfcbx_allefffectstable.Category)));
catinfo(5,2) = (std(pc1mapcorrs_abs(strcmpi('Mental Health',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Mental Health',rsfcbx_allefffectstable.Category))));

catinfo(6,1) = mean(pc1mapcorrs_abs(strcmpi('Demographics',rsfcbx_allefffectstable.Category)));
catinfo(6,2) = (std(pc1mapcorrs_abs(strcmpi('Demographics',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Demographics',rsfcbx_allefffectstable.Category))));

catinfo(7,1) = mean(pc1mapcorrs_abs(strcmpi('Culture/Environment',rsfcbx_allefffectstable.Category)));
catinfo(7,2) = (std(pc1mapcorrs_abs(strcmpi('Culture/Environment',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Culture/Environment',rsfcbx_allefffectstable.Category))));

catinfo(8,1) = mean(pc1mapcorrs_abs(strcmpi('Medical History',rsfcbx_allefffectstable.Category)));
catinfo(8,2) = (std(pc1mapcorrs_abs(strcmpi('Medical History',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Medical History',rsfcbx_allefffectstable.Category))));

catinfo(9,1) = mean(pc1mapcorrs_abs(strcmpi('Parenting',rsfcbx_allefffectstable.Category)));
catinfo(9,2) = (std(pc1mapcorrs_abs(strcmpi('Parenting',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Parenting',rsfcbx_allefffectstable.Category))));

catinfo(10,1) = mean(pc1mapcorrs_abs(strcmpi('Personality',rsfcbx_allefffectstable.Category)));
catinfo(10,2) = (std(pc1mapcorrs_abs(strcmpi('Personality',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Personality',rsfcbx_allefffectstable.Category))));

catinfo(11,1) = mean(pc1mapcorrs_abs(strcmpi('Social Adjustment',rsfcbx_allefffectstable.Category)));
catinfo(11,2) = (std(pc1mapcorrs_abs(strcmpi('Social Adjustment',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Social Adjustment',rsfcbx_allefffectstable.Category))));

catinfo(12,1) = mean(pc1mapcorrs_abs(strcmpi('Substance Use',rsfcbx_allefffectstable.Category)));
catinfo(12,2) = (std(pc1mapcorrs_abs(strcmpi('Substance Use',rsfcbx_allefffectstable.Category)))) ./ ...
    sqrt(length(pc1mapcorrs_abs(strcmpi('Substance Use',rsfcbx_allefffectstable.Category))));

cmap = [31 120 180]./255;
cmap(2,:) = [116 196 118]./255;
cmap(3,:) = [0 0 118]./255;
cmap(4,:) = [10 107 10]./255;
cmap(5,:) = [158 154 200]./255;
cmap(6,:) = [255 255 127]./255;
cmap(7,:) = [166 206 227]./255;
cmap(8,:) = [227 26 28]./255;
cmap(9,:) = [251 154 153]./255;
cmap(10,:) = [106 61 154]./255;
cmap(11,:) = [253 191 111]./255;
cmap(12,:) = [0 128 128]./255;

figure; hold on
rng(0,'twister');
for i = 1:12
   h=errorbar((-.05 - .05).*rand(1,1) + 0.05 , catinfo(i,1) , catinfo(i,2));
   h.Color = cmap(i,:); h.LineWidth = 2.5;
end
xlim([-.7 .7])

% Make PC1 brain map 
pc1map = mapscores(:,1);
mat = ones(333);
uidx = find(triu(mat,1));
lidx = find(tril(mat));
mat(uidx) = pc1map;
mat(lidx) = 0;
mat = mat + mat';
mat(eye(size(mat))==1) = nan;
out = nansum(abs(mat));
outn = ((out-min(out)) ./ (max(out) - min(out)))';
tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
outvec = zeros(size(tf.data,1),2);
for r = 1:333
    idx=length(find(tf.data == r));
    % ses
    this = [];
    this = outn(r,1);
    this = repmat(this,idx,1);
    outvec(tf.data==r,1) = this;
end
tf.data(1:59412,1:2) = outvec;
%tf.data(:,1) = tf.data(:,1) - .01; tf.data(:,1)
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/pc1_edges',tf)

% Quantify by network
% quantify cv1 by network
network_ses = [];
parcid = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
nets = ft_read_cifti_mod('/data/nil-bluearc/GMT/Evan/Published_parcels/Parcel_Communities_wSCAN.dtseries.nii');
parcid = parcid.data(1:59412,1);
nets = nets.data(1:59412,1);
netid = [];

for i = 1:333
    theseverts = find(parcid == i);
    netid(i,1) = nets(theseverts(2),1);
end
idx = find(netid==1.5);
netid(idx) = 4; % SCAN
network_ses = zeros(16,1);
for i = [1 2 3 4 5 7 8 9 10 11 12 15 16]
         network_ses(i,1) = nanmean(out(netid == i,1));
end

% Make scattergram
[~ , sidx] = sort(network_ses,'descend');
netvec = sidx(1:13);
cmap(1,:) = [255 36 37 ]./255;
cmap(2,:) = [ 36 37 167 ]./255;
cmap(3,:) = [ 247 247 37 ]./255;
cmap(4,:) = [ 110 37 80 ]./255;
cmap(5,:) = [ 38 209 38 ]./255;
cmap(7,:) = [ 37 161 161 ]./255;
cmap(8,:) = [ 37 37 37 ]./255;
cmap(9,:) = [ 93 37 157 ]./255;
cmap(10,:) = [ 70 228 227 ]./255;
cmap(11,:) = [ 253 136 38 ]./255;
cmap(12,:) = [ 162 76 255 ]./255;
cmap(15,:) = [ 36 37 255 ]./255;
cmap(16,:) = [ 255 255 220 ]./255;
figure; hold on 
for n = 1:length(netvec)
    k = scatter(n-1 + ones(length(out(netid==netvec(n),1)),1) , out(netid==netvec(n),1));
    k.MarkerFaceColor = [0 0 0]; k.MarkerFaceAlpha = .15;
    k.MarkerEdgeColor = 'k'; k.MarkerEdgeAlpha = .75;
    l = scatter(n,mean(out(netid==netvec(n),1)));
    l.MarkerFaceColor = cmap(netvec(n),:); l.MarkerFaceAlpha = 1;
    l.MarkerEdgeColor = cmap(netvec(n),:); l.MarkerEdgeAlpha = 1;
    l.SizeData = 100;
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

% t-test of sensorimotor vs. control/attention
smnets = find(netid == 2 | netid == 4 | netid == 10 | netid == 11 | netid == 12);
canets = find(netid == 3 | netid == 5 | netid == 7 | netid == 8 | netid == 9 | netid == 1 | netid == 15 | netid == 16);
sesout = makebrainmap(rsfcdiscphewasoutput.allbrbxphewas_noabs(:,588),'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/coibrainmap');
sesout = sesout';
[h p ci stats] = ttest2(sesout(smnets),sesout(canets));

%% make new 4a (out of sample correlation (CCA) using full ABCD sample 

load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')
nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);

pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

yrep = [ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ];


% Grab a subsample of 579 to match panel B (high ses people)
for i = 1:100
    idx = randperm(2316);
    idx = idx(1:579);
    [oosr,insampr,h] = runcca(rsfcpmt(idx,:),y(idx,:),rsfcpmtrep,yrep);
    h.MarkerFaceColor = [0 109 44]./255;
    h.MarkerEdgeColor = [0 109 44]./255;
    h.MarkerFaceAlpha = .05;
    j = lsline; j.LineWidth = 2.5; j.Color = 'k';
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
    ylim([-5 3]); xlim([-4 4])
    alloosr(i,1) = oosr;
end

% Train on high ses only, fit to full (4b)
ses = zscore(cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat));
braintrain = rsfcpmt(ses > .75,:);
ytrain = y(ses > 0.75,:);

[oosr,insampr,h] = runcca(braintrain,ytrain,rsfcpmtrep,yrep);
h.MarkerFaceColor = [0 109 44]./255;
h.MarkerEdgeColor = [0 109 44]./255;
h.MarkerFaceAlpha = .05;
j = lsline; j.LineWidth = 2.5; j.Color = 'k';
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
ylim([-5 3]); xlim([-4 4])


%% Fig 5c, within ABCD generalizability 
load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);

pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

y = [ pmtable.nihtbx_cardsort_uncorrected , pmtable.nihtbx_flanker_uncorrected ...
pmtable.nihtbx_list_uncorrected , pmtable.nihtbx_pattern_uncorrected , ...
pmtable.nihtbx_picture_uncorrected , pmtable.nihtbx_picvocab_uncorrected , ...
pmtable.nihtbx_reading_uncorrected ];

yrep = zscore([ pmtablerep.nihtbx_cardsort_uncorrected , pmtablerep.nihtbx_flanker_uncorrected ...
pmtablerep.nihtbx_list_uncorrected , pmtablerep.nihtbx_pattern_uncorrected , ...
pmtablerep.nihtbx_picture_uncorrected , pmtablerep.nihtbx_picvocab_uncorrected , ...
pmtablerep.nihtbx_reading_uncorrected ]);

ses = zscore(cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat));
pfactor = zscore(pmtable.TotalProblems);

%samplingvectorses = flipud([-3.65:.14:.65]');
% samplingvectorses = flipud([-2.58:.035:.80]'); % ADI raw
%samplingvectorp = [-.5:0.04:2.96]'; % for p
samplingvectorp = [-.5:0.115:2.96]'; % for p
%abcdoosr_ses = zeros(100,31);
abcdoosr_p = zeros(100,31);
rsfcrep = rsfcpmtrep;

z=parpool(4);
parfor t = 1:length(samplingvectorses)
    disp(['On interation ' num2str(t)])
    %thisabcdoosr_ses = zeros(100,1);
    thisabcdoosr_p = zeros(100,1);
    %thrses = samplingvectorses(t);
    thrp = samplingvectorp(t);
     % define abcd sample
%     thisxses = rsfcpmt(ses > thrses,:);
%     thisyses = y(ses > thrses,:);
%     
    thisxp = rsfcpmt(pfactor < thrp,:);
    thisyp = y(pfactor < thrp,:);

    for i = 1:100
        
        % SES 
%        thisidx=datasample(1:size(thisxses,1),500)';
%          thisidx=randperm(size(thisxses,1))';
%          thisidx = thisidx(1:500);
%         % subsample 1000 individuals from distribution
%         thisxidx = zscore(thisxses(thisidx,:));
%         thisyidx = zscore(thisyses(thisidx,:));
% 
%         [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
%         rsfc_varexplained = cumsum(explained);
%         % Using components accounting for 20% variance exaplained for prediction
%         fn = 20;
%         fn = find(rsfc_varexplained>fn,1);
%         x = brainscores(:,1:fn);
%         [A B r U V] = canoncorr(x,thisyidx); 
% 
%         
%         brainscores_rep = (rsfcrep - mu)*coeffs;
%         xun = brainscores_rep(:,1:fn);
%         thisabcdoosr_ses(i,1) = corr(xun*A(:,1),yrep*B(:,1));
%         
%         % P factor foil
%        thisidx=datasample(1:size(thisxp,1),650)';
          thisidx=randperm(size(thisxp,1))';
          thisidx = thisidx(1:500);
       
        % subsample 1000 individuals from distribution
        thisxidx = zscore(thisxp(thisidx,:));
        thisyidx = zscore(thisyp(thisidx,:));

        [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
        rsfc_varexplained = cumsum(explained);
        % Using components accounting for 20% variance exaplained for prediction
        fn = 20;
        fn = find(rsfc_varexplained>fn,1);
        x = brainscores(:,1:fn);
        [A B r U V] = canoncorr(x,thisyidx); 

        
        brainscores_rep = (rsfcrep - mu)*coeffs;
        xun = brainscores_rep(:,1:fn);
        thisabcdoosr_p(i,1) = corr(xun*A(:,1),yrep*B(:,1));
        
        
    end
    %disp([' mean on bin ' num2str(t) ' is ' num2str(mean(hcpoosr,1))])
    abcdoosr_p(:,t) = thisabcdoosr_p;
    %abcdoosr_ses(:,t) = thisabcdoosr_ses;
end
delete(z)
% hcpoosr = [hcpoosr ; hcpoosrb];
% mhcpoosr = mean(hcpoosr,1);
% figure;plot(mhcpoosr);

mabcdoosr = mean(abcdoosr_ses,1);
figure;hold on
lineProps.col = {[25 88 73]./255};
mseb(1:length(samplingvectorses),mabcdoosr,.5*(std(abcdoosr_ses,0,1)),lineProps,.2);

mabcdoosr = mean(abcdoosr_p,1);
lineProps.col = {[6 131 139]./255};
mseb(1:length(samplingvectorses),mabcdoosr,.5*(std(abcdoosr_p,0,1)),lineProps,.2);
ylim([0 .25]); xlim([1 87])
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')

%% Varing training sample of brain with ses by p-factor 
%% Fig 5c, within ABCD generalizability 
load('pmtable.mat')
load('pmtablerep.mat')
load('rsfcpmtrep.mat')
load('rsfcpmt.mat')

nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);
nanidxrep = sum(isnan([cellfun(@str2double,pmtablerep.reshist_addr1_coi_z_se_nat) , pmtablerep.TotalCogAbility , pmtablerep.TotalProblems , cellfun(@str2double,pmtablerep.stq_y_ss_weekday) , cellfun(@str2double,pmtablerep.stq_y_ss_weekend) , pmtablerep.FamilyIncome , pmtablerep.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtablerep.demo_prnt_marital_v2b)]),2);
nanidxrep = logical(nanidxrep);

pmtable(nanidx==1,:) = [];
pmtablerep(nanidxrep==1,:) = [];

y = [ cellfun(@str2double , pmtable.reshist_addr1_coi_z_se_nat) pmtable.FamilyIncome];

yrep = [ cellfun(@str2double , pmtablerep.reshist_addr1_coi_z_se_nat) pmtablerep.FamilyIncome];

pfactor = zscore(pmtable.TotalProblems);


samplingvectorp = [-.5:0.115:2.96]'; % for p
abcdoosr_sesvaryp = zeros(100,31);
rsfcrep = rsfcpmtrep;

z=parpool(4);
parfor t = 1:length(samplingvectorp)
    disp(['On interation ' num2str(t)])
    thisabcdoosr_p = zeros(100,1);
    thrp = samplingvectorp(t);
     % define abcd sample
    
    thisxp = rsfcpmt(pfactor < thrp,:);
    thisyp = y(pfactor < thrp,:);

    for i = 1:100
        
        % SES 
%        thisidx=datasample(1:size(thisxses,1),500)';
         thisidx=randperm(size(thisxp,1))';
         thisidx = thisidx(1:500);
        % subsample 500 individuals from distribution
        thisxidx = zscore(thisxp(thisidx,:));
        thisyidx = zscore(thisyp(thisidx,:));

        [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
        rsfc_varexplained = cumsum(explained);
        % Using components accounting for 20% variance exaplained for prediction
        fn = 20;
        fn = find(rsfc_varexplained>fn,1);
        x = brainscores(:,1:fn);
        [A B r U V] = canoncorr(x,thisyidx); 

        
        brainscores_rep = (rsfcrep - mu)*coeffs;
        xun = brainscores_rep(:,1:fn);
        thisabcdoosr_p(i,1) = corr(xun*A(:,1),yrep*B(:,1));
        
%         % P factor foil
%         thisidx=datasample(1:size(thisxp,1),650)';
% %          thisidx=randperm(size(thisxp,1))';
% %          thisidx = thisidx(1:877);
%        
%         % subsample 1000 individuals from distribution
%         thisxidx = zscore(thisxp(thisidx,:));
%         thisyidx = zscore(thisyp(thisidx,:));
% 
%         [coeffs,brainscores,~,~,explained,mu] = pca(thisxidx);
%         rsfc_varexplained = cumsum(explained);
%         % Using components accounting for 20% variance exaplained for prediction
%         fn = 20;
%         fn = find(rsfc_varexplained>fn,1);
%         x = brainscores(:,1:fn);
%         [A B r U V] = canoncorr(x,thisyidx); 
% 
%         
%         brainscores_rep = (rsfcrep - mu)*coeffs;
%         xun = brainscores_rep(:,1:fn);
%         thisabcdoosr_p(i,1) = corr(xun*A(:,1),yrep*B(:,1));
        
        
    end
    %disp([' mean on bin ' num2str(t) ' is ' num2str(mean(hcpoosr,1))])
    abcdoosr_sesvaryp(:,t) = thisabcdoosr_p;
end
delete(z)


%% Fig 4. 
load('ct_cogses_unadj_adj.mat')
load('rsfctstats.mat')

% IQ
edges = [0:.1:6];
% Make scatter plot of the association between t-statistics - RSFC pm;y 
figure; hold on;
h = histogram(abs([rsfctstats.cogunadj]),edges);
h.FaceColor = [150 150 150]./255;
h.EdgeColor = [150 150 150]./255;
z = histogram(abs([rsfctstats.cogadj]),edges);
z.FaceColor = [65 171 93]./255;
z.EdgeColor = [65 171 93]./255;
z.FaceAlpha = .4; z.EdgeAlpha = .4;
xlabel('|t-statistic|')
ylabel('Frequency')
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/rsfc_cog_sesadj_v_unadj','png')

% Percent of associations that decrease effect size 
rsfcunadj = abs(rsfctstats.cogunadj(abs(rsfctstats.cogunadj)>3));
rsfcadj = abs(rsfctstats.cogadj(abs(rsfctstats.cogunadj)>3));

% Append cortical thickness
% Percent of associations that decrease effect size 
ctunadj = abs(ctcogses.cogtstatunadj(abs(ctcogses.cogtstatunadj)>3));
ctadj = abs(ctcogses.cogtstatadj(abs(ctcogses.cogtstatunadj)>3));

unadj = [rsfcunadj];
adj = [rsfcadj];

difference = adj - unadj; % 100.*(length(find(difference<0)) / length(difference))

% Percent of adj associations that are sig unadj & >= unadj value
sig_noinflation = 100*(length(find(difference>=0)) / length(difference));

% Percent of adj associations that are no longer sig 
nosig = 100*(length(find(adj<3)) / length(adj));

% Percent of adj associations that are sig unadj & < unadj value (the
% remainder)
sig_wasinflated = 100-sig_noinflation-nosig;

disp(['Percent significant and not inflated = ' num2str(sig_noinflation)])
disp(['Percent significant but were inflated = ' num2str(sig_wasinflated)])
disp(['Percent no longer significant = ' num2str(nosig)])

figure;h=scatter(unadj,adj-unadj);
j=lsline;
j.Color = [.2 .6 .2]; 
j.LineWidth = 2.5;
h.Marker = 'o';
h.SizeData = 50;
h.MarkerEdgeColor = 'k';
h.MarkerFaceAlpha = .1;
ylim([-3 1])
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/rsfc_cog_sesadj_v_unadjdiff','svg')

% SES - opportunity
edges = [0:.1:7];
% Make scatter plot of the association between t-statistics - RSFC & CT 
figure; hold on;
h = histogram(abs([rsfctstats.sesunadj]),edges);
h.FaceColor = [150 150 150]./255;
h.EdgeColor = [150 150 150]./255;
z = histogram(abs([rsfctstats.sesadj]),edges);
z.FaceColor = [65 171 93]./255;
z.EdgeColor = [65 171 93]./255;
z.FaceAlpha = .4; z.EdgeAlpha = .4;
xlabel('|t-statistic|')
ylabel('Frequency')
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/rsfc_ses_cogadj_v_unadj','png')

% Percent of associations that decrease effect size 
rsfcunadj = abs(rsfctstats.sesunadj(abs(rsfctstats.sesunadj)>3));
rsfcadj = abs(rsfctstats.sesadj(abs(rsfctstats.sesunadj)>3));

% Append cortical thickness
% Percent of associations that decrease effect size 
% ctunadj = abs(ctcogses.seststatunadj(abs(ctcogses.seststatunadj)>3));
% ctadj = abs(ctcogses.seststatadj(abs(ctcogses.seststatunadj)>3));

unadj = [rsfcunadj ];
adj = [rsfcadj ];

difference = adj - unadj; % 100.*(length(find(difference<0)) / length(difference))

% Percent of adj associations that are sig unadj & >= unadj value
sig_noinflation = 100*(length(find(difference>=0)) / length(difference));

% Percent of adj associations that are no longer sig 
nosig = 100*(length(find(adj<3)) / length(adj));

% Percent of adj associations that are sig unadj & < unadj value (the
% remainder)
sig_wasinflated = 100-sig_noinflation-nosig;

disp(['Percent significant and not inflated = ' num2str(sig_noinflation)])
disp(['Percent significant but were inflated = ' num2str(sig_wasinflated)])
disp(['Percent no longer significant = ' num2str(nosig)])

figure;h=scatter(unadj,adj-unadj);
j=lsline;
j.Color = [.2 .6 .2]; 
j.LineWidth = 2.5;
h.Marker = 'o';
h.SizeData = 50;
h.MarkerEdgeColor = 'k';
h.MarkerFaceAlpha = .1;
ylim([-3 1])
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
saveas(gcf,'/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/matlabsvg/rsfc_ses_cogadj_v_unadjdiff','svg')

%% regress COI from each variable and rerun 
load('pmtable.mat')
load('rsfcpmt.mat')
load('behvars_disc.mat')
genetics = readtable('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/geneticpcs.txt');
nanidx = sum(isnan([cellfun(@str2double,pmtable.reshist_addr1_coi_z_se_nat) , pmtable.TotalCogAbility , pmtable.TotalProblems , cellfun(@str2double,pmtable.stq_y_ss_weekday) , cellfun(@str2double,pmtable.stq_y_ss_weekend) , pmtable.FamilyIncome , pmtable.sleepdisturb1_p_behdisc , cellfun(@str2double,pmtable.demo_prnt_marital_v2b)]),2);
nanidx = logical(nanidx);

behvars(nanidx,:) = []; 
pmtable(nanidx,:) = [];
sescoi = cellfun(@str2double,behvars.reshist_addr1_coi_z_se_nat);
fd = pmtable.FD;
genpc1 = pmtable.genetic_pc_1;
lifefactors = zeros(size(behvars));
for b = 1:size(behvars,2)
    if isa(table2array(behvars(:,b)),'cell')
        thisbeh = cellfun(@str2double, table2array(behvars(:,b)));
    else
        thisbeh = table2array(behvars(:,b));
    end
    [~,~,thisbehr] = regress(thisbeh , [ones(size(behvars,1),1) genpc1 sescoi]);
    lifefactors(:,b) = thisbehr;
end

allbrbx_resid = nan(size(rsfcpmt,2),size(behvars,2));
z = parpool(4);
parfor b = 1:size(behvars,2)
    ['On interation ' num2str(b)]
    thisbeh = lifefactors(:,b);
    thisvec = zeros(size(rsfcpmt,2),1);
    for i = 1:size(rsfcpmt,2)
        thisvec(i,1) = corr(rsfcpmt(:,i),thisbeh,'rows','complete');
    end
    allbrbx_resid(:,b) = thisvec;
end
delete(z)
% compare
load('rsfcbx_table_discpluscombatplusrep.mat'); load ('rsfcdiscphewasoutput.mat');
top1percgenses = prctile(abs(allbrbx_resid),99)';
t1psgenses = top1percgenses(rsfcdiscphewasoutput.sortidx);
rsfcbx_allefffectstable = addvars(rsfcbx_allefffectstable,t1psgenses);

% sort by category
% life factor correlation with ses - effect size in brain
figure;hold on; 
for s = 1:size(rsfcbx_allefffectstable,1)
    if ~strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
        k=scatter(rsfcbx_allefffectstable.lf_w_ses(s,1),rsfcbx_allefffectstable.t1psgenses(s));
        k.Marker = '.'; k.SizeData = 500;
        if strcmpi(rsfcbx_allefffectstable.Category{s},'Cognition')
                k.MarkerFaceColor = [116 196 118]./255;
                k.MarkerEdgeColor = [116 196 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Culture/Environment')
                k.MarkerFaceColor = [166 206 227]./255;
                k.MarkerEdgeColor = [166 206 227]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Physical Health')
                k.MarkerFaceColor = [10 107 10]./255;
                k.MarkerEdgeColor = [10 107 10]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Mental Health')
                k.MarkerFaceColor = [158 154 200]./255;
                k.MarkerEdgeColor = [158 154 200]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Medical History')
                k.MarkerFaceColor = [227 26 28]./255;
                k.MarkerEdgeColor = [227 26 28]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Screen Time')
                k.MarkerFaceColor = [0 0 118]./255;
                k.MarkerEdgeColor = [0 0 118]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Demographics')
                k.MarkerFaceColor = [255 255 127]./255;
                k.MarkerEdgeColor = [255 255 127]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Parenting')
                k.MarkerFaceColor = [251 154 153]./255;
                k.MarkerEdgeColor = [251 154 153]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Personality')
                k.MarkerFaceColor = [106 61 154]./255;
                k.MarkerEdgeColor = [106 61 154]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Social Adjustment')
                k.MarkerFaceColor = [253 191 111]./255;
                k.MarkerEdgeColor = [253 191 111]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Socioeconomic')
                k.MarkerFaceColor = [31 120 180]./255;
                k.MarkerEdgeColor = [31 120 180]./255;
            elseif strcmpi(rsfcbx_allefffectstable.Category{s},'Substance Use')
                k.MarkerFaceColor = [0 128 128]./255;
                k.MarkerEdgeColor = [0 128 128]./255;
        end
    else
        a = 1;
    end
end
set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')


%% Ridge supplemental plotting
ll = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/low_model_low_test.csv');
lf = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/low_model_full_test.csv');
lh = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/low_model_high_test.csv');

hl = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/high_model_low_test.csv');
hf = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/high_model_full_test.csv');
hh = readtable('/net/10.20.145.34/DOSENBACH02/GMT2/Andrew/cogtest/results/high_model_high_test.csv');

%plot
figure; hold on
subplot(2,3,1)
h=scatter(zscore(ll.predicted),zscore(ll.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(ll.predicted),zscore(ll.actual))));

subplot(2,3,2)
h=scatter(zscore(lf.predicted),zscore(lf.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(lf.predicted),zscore(lf.actual))));

subplot(2,3,3)
h=scatter(zscore(lh.predicted),zscore(lh.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(lh.predicted),zscore(lh.actual))));

subplot(2,3,4)
h=scatter(zscore(hl.predicted),zscore(hl.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(hl.predicted),zscore(hl.actual))));

subplot(2,3,5)
h=scatter(zscore(hf.predicted),zscore(hf.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(hf.predicted),zscore(hf.actual))));

subplot(2,3,6)
h=scatter(zscore(hh.predicted),zscore(hh.actual)); j = lsline; j.LineWidth = 2.5; j.Color = 'k';
h.MarkerFaceColor = [161 217 155]./255;
h.MarkerEdgeColor = [0 109 44]./255;
title(num2str(corr(zscore(hh.predicted),zscore(hh.actual))));

for i = 1:6
    subplot(2,3,i)
    xlim([-5 5]); ylim([-5 5]);
    set(gca,'FontSize',12,'LineWidth',2,'FontName','Arial')
end




%% arousal 
eyesclosed = ft_read_cifti_mod('/data/cn5/PoldrackData/Poldrome/WashU_data/FCPROCESS_SCRUBBED_UWRPMEAN/cifti_correlation_normalwall/poldrack_washu.eyes_closed.allsubs_LR_timeseries_corr.dconn.nii');
eyesopen = ft_read_cifti_mod('/data/cn5/PoldrackData/Poldrome/WashU_data/FCPROCESS_SCRUBBED_UWRPMEAN/cifti_correlation_normalwall/poldrack_washu.eyes_open.allsubs_LR_timeseries_corr.dconn.nii');
diff = eyesopen.data - eyesclosed.data;
openminusclose = eyesopen; 
openminusclose.data = diff; 
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/eyesopenminusclosed_rp',openminusclose);

diff = eyesopen.data(1:59412,1:59412) - eyesclosed.data(1:59412,1:59412);
diffsum = sum(abs(diff(1:59412,1:59412)));
diffsum = diffsum';
tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
tf.data(1:59412,1) = diffsum;
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/eyesopencloseddiffsumverts',tf);


tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
parcelmean = zeros(59412,1);
parmat = zeros(333);
for i = 1:max(tf.data)
    for j = i+1:max(tf.data)
    thisroi_i = find(tf.data == i);
    thisroi_j = find(tf.data ==j);
    vals = diff(thisroi_i,thisroi_j);
    parmat(i,j) = mean(mean(vals));
    end
end
parmat = parmat + parmat';
figure;imagesc(parmat)

parmat(i,:) = mean(sum(abs(diff(thisroi,:)')));
    parcelmean(thisroi,1) = roimean(i,1);

tf.data = parcelmean;
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/eyesopencloseddiffavgrois',tf);


tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');
pc1=ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/COIsemap.dtseries.nii');
ampm = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/MotorSeeds_am_pm.dtseries.nii');
for j = 1:59412
thisdiffmap = diff(:,29289);
for i = 1:max(tf.data)
    thisroi = find(tf.data == i); 
%     thisroi = thisroi(1);
%     pc1parcel(i,1) = pc1.data(thisroi,1);
%     ampmparcels(i,1) = ampm.data(thisroi,1);
    ec(i,1) = sum(abs(thisdiffmap(thisroi,1)));
end
corrtoses(j,1) = corr(pc1parcel,ec);
end
%roinorm = ((roimean-min(roimean)) ./ (max(roimean) - min(roimean)));
corr(pc1parcel,ec)


%% norepinephrine map 

NEmap = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/data/NAT_MRB_hc77_ding_smooth12.dtseries.nii');

tf = ft_read_cifti_mod('/data/nil-bluearc/GMT/Scott/Parcels/Parcels_LR.dtseries.nii');

for i = 1:max(tf.data)
    thisroi = find(tf.data == i); 
    NEparcels(i,1) = mean(NEmap.data(thisroi,1));
end

mat = ones(333);
uidx = find(triu(mat,1));
mat = zeros(333);
mat(uidx) = sescorr;
mat = mat + mat';
mat = sum(mat)';

NEmap.data(:,2) = zeros(size(NEmap.data,1),1);
for i = 1:333
    idx = find(tf.data == i);
    NEmap.data(idx,2) = repmat(NEparcels(i,1),length(idx),1);
end
NEmap.data(:,2) = abs(NEmap.data(:,2));
ft_write_cifti_mod('/data/nil-bluearc/GMT/Scott/ABCD/Ses_cog/NEmap',NEmap);
